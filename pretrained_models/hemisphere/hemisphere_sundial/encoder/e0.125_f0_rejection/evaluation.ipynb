{
 "cells": [
  {
   "cell_type": "markdown",
   "source": [
    "In this notebook, we use a pretrained encoder trained on the hemisphere dataset with $\\lambda=0$ and visualize the latent space."
   ],
   "metadata": {
    "collapsed": false
   }
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# adapt width of notebook\n",
    "from IPython.core.display import display, HTML\n",
    "\n",
    "display(HTML(\"<style>.container { width:70% !important; }</style>\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# add code folder to python path, such that the custom code is found\n",
    "from pathlib import Path\n",
    "import sys\n",
    "\n",
    "load_path = Path(globals()[\"_dh\"][0])\n",
    "code_path = load_path.parents[4]\n",
    "sys.path.insert(0, str(code_path))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from lbad.data.manifold.hemisphere import Hemisphere\n",
    "from lbad.data.structures import PrecomputedDatasetWithTransform, MultiIndexDataLoader\n",
    "from lbad.utils import batched_forward, to_torch_and_device\n",
    "from lbad.utils.training import setup_networks\n",
    "from lbad.utils import get_num_components\n",
    "from lbad.utils.plotting.latentfigures import set_limit_and_aspect, make_latent_space_figure\n",
    "\n",
    "from scripts.constants import RENDERER\n",
    "from lbad.utils.plotting import fig2img"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import torch\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.utils.extmath import stable_cumsum\n",
    "from sklearn.decomposition import PCA"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "import pprint\n",
    "\n",
    "# load parameters corresponding to experiment\n",
    "with open(f\"{load_path}/args.json\", \"r\") as f:\n",
    "    args = json.load(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# print parameters of the experiment\n",
    "pprint.pprint(args)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# device on which evaluation is performed\n",
    "device = \"cuda:0\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# extract parameters concerning the data\n",
    "args_data = args[\"data\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Initialize the underlying manifold, in this case the hemisphere.\n",
    "This will be useful later for visualizing specific points on the hemisphere in latent space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "manifold = Hemisphere(**args_data.get(\"manifold_args\", {}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Initialize the renderer, which maps a point on the manifold to a pixel image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "renderer_name = args_data[\"renderer\"]\n",
    "renderer = RENDERER[renderer_name](**args_data[\"renderer_args\"], device=device)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Initialize a test dataset. Test points are stored as points on the hemisphere in a pickle file.\n",
    "A `transform` is used to map those points to images.\n",
    "This way, we don't have to store the pixel images, and we can easily replace the renderer while keeping the underlying points.\n",
    "\n",
    "We then plug the dataset into a `DataLoader`, which yields batches of images, since the whole dataset might not fit into memory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset_test = PrecomputedDatasetWithTransform(\n",
    "    load_path=code_path / \"test_data\" / \"hemisphere_max_30000.pickle\",\n",
    "    num_samples=None,\n",
    "    transform=lambda x: renderer.render(to_torch_and_device(x, device)),\n",
    "    sample_tuples=False,\n",
    ")\n",
    "dataloader_test = MultiIndexDataLoader(dataset_test, batch_size=128, shuffle=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now set up the encoder, using the parameters concerning the architecture."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "encoder = setup_networks(args[\"architecture\"], include_decoder=False)\n",
    "encoder.to(device);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We load the pretrained weights."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "state_dict = torch.load(f\"{load_path}/state_dict_encoder_early_stop_best.pth\")\n",
    "encoder.load_state_dict(state_dict)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the notebook is run for the first time, the latent codes corresponding to the test dataset are computed. They are then stored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    latent_codes = np.load(f\"{load_path}/latent_points_random.npy\")\n",
    "except FileNotFoundError:\n",
    "    with torch.no_grad():\n",
    "        latent_codes = batched_forward(dataloader_test, encoder)\n",
    "    np.save(f\"{load_path}/latent_points_random.npy\", latent_codes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now fit a PCA to the computed latent codes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca = PCA()\n",
    "pca.fit(latent_codes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the next step, the first component is flipped.\n",
    "We found this gave a nicer visualization in this case.\n",
    "Alternatively, one may also compute a transformation such that the orientation of the hemisphere is as desired, but we found this was not necessary in this case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "pca.components_[0] = -pca.components_[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Transform the latent codes to coordinates with respect to PCA components."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pca_codes = pca.transform(latent_codes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "print(\n",
    "    f\"Amount of variance explained by each of the components:\\n \"\n",
    "    f\"{np.array_str(pca.explained_variance_ratio_, precision=4)}\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "pycharm": {
     "is_executing": true
    }
   },
   "outputs": [],
   "source": [
    "print(\n",
    "    f\"Amount of variance explained by the first n components: \\n \"\n",
    "    f\"{np.array_str(stable_cumsum(pca.explained_variance_ratio_), precision=4)}\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "num_components = get_num_components(pca.explained_variance_ratio_, 0.99)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f\"Number of components needed to explain 99% of the variance: {num_components}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(stable_cumsum(pca.explained_variance_ratio_))\n",
    "plt.title(\"Total amount of explained variance as a function \\n of increasing subspace dimension\")\n",
    "plt.xlabel(\"Subspace dimension\")\n",
    "plt.ylabel(\"Explained variance in percent\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "plt.plot(pca.explained_variance_)\n",
    "plt.title(\"Amount of explained variance by each of the components\")\n",
    "plt.xlabel(\"Component number\")\n",
    "plt.ylabel(\"Explained variance\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# store the computed values as dataframes\n",
    "pd.DataFrame(data=pca.explained_variance_, columns=[\"var\"]).to_csv(load_path / \"exp_var.csv\", index_label=\"c\")\n",
    "pd.DataFrame(data=np.cumsum(pca.explained_variance_ratio_), columns=[\"var\"]).to_csv(\n",
    "    load_path / \"exp_var_sum.csv\", index_label=\"c\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualize the latent manifold, we add some specific points.\n",
    "We specify them using azimuth and polar angles (\"intrinsic coordinates\") and convert them to coordinates in $\\mathbb{R}^3$ (\"extrinsic coordinates\").\n",
    "We then render them as pixel images, map them to latent space and apply the PCA transform.\n",
    "When rendering the images, we need to specify `plotting_format=False`, so that the images have the correct shape expected from the encoder.\n",
    "Since the encoder outputs the latent codes with shape `(batch_size, 1, 4, 4)`, we need to reshape the encoder output before applying the PCA."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "additional_points = manifold.intrinsic_to_extrinsic(\n",
    "    azimuth_angle=torch.tensor([0, 0.35, 0.2, 0.6, 0.9]) * np.pi * 2,\n",
    "    polar_angle=torch.tensor([0.8, 0.95, 0.6, 0.7, 0.5]) * np.pi / 2,\n",
    ")\n",
    "additional_images = renderer.render(to_torch_and_device(additional_points), plotting_format=False)\n",
    "additional_latent_codes = encoder(additional_images).reshape(len(additional_images), -1).cpu().detach().numpy()\n",
    "additional_pca_codes = pca.transform(additional_latent_codes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We further choose some triples by defining start and end points with different distances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "start_points = manifold.intrinsic_to_extrinsic(\n",
    "    azimuth_angle=torch.tensor([0.1, 0.4, 0.95]) * np.pi * 2, polar_angle=torch.tensor([0.95, 0.9, 0.3]) * np.pi / 2\n",
    ")\n",
    "start_images = renderer.render(to_torch_and_device(start_points), plotting_format=False)\n",
    "start_latent_codes = encoder(start_images).reshape(len(start_images), -1).cpu().detach().numpy()\n",
    "start_pca_codes = pca.transform(start_latent_codes)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "end_points = manifold.intrinsic_to_extrinsic(\n",
    "    azimuth_angle=torch.tensor([0.25, 0.5, 0.9]) * np.pi * 2, polar_angle=torch.tensor([0.95, 0.7, 0.9]) * np.pi / 2\n",
    ")\n",
    "end_images = renderer.render(to_torch_and_device(end_points), plotting_format=False)\n",
    "end_latent_codes = encoder(end_images).reshape(len(end_images), -1).cpu().detach().numpy()\n",
    "end_pca_codes = pca.transform(end_latent_codes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use the manifold to compute the ground truth midpoint between the start and end points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "middle_points = manifold.midpoint(to_torch_and_device(start_points), to_torch_and_device(end_points))\n",
    "middle_images = renderer.render(middle_points, plotting_format=False)\n",
    "middle_latent_codes = encoder(middle_images).reshape(len(middle_images), -1).cpu().detach().numpy()\n",
    "middle_pca_codes = pca.transform(middle_latent_codes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we use the function `make_latent_space_figure`, which plots the computed pca codes of the test dataset as a point cloud, with coloring according to the z-coordinate.\n",
    "It further draws a spline between our start, end and middle points and adds the additional points in a different color.\n",
    "We configure the bounds and adapt the aspect ratio accordingly using the `set_limit_and_aspect` function.\n",
    "We further find a nice rotation an initialize the view with this.\n",
    "Finally, the function `fig2img` converts the figure to a PIL image while cropping away transparent regions, so that we can save the figure as an image without additional space.\n",
    "\n",
    "We first use bounds that work for both $\\lambda=0$ and $\\lambda=10$ (determined experimentally)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig = make_latent_space_figure(\n",
    "    pca_codes,\n",
    "    points=additional_pca_codes,\n",
    "    start=start_pca_codes,\n",
    "    middle=middle_pca_codes,\n",
    "    end=end_pca_codes,\n",
    "    opacity=0.2,\n",
    "    s=2,\n",
    "    annotate_points=False,\n",
    "    figsize=(8, 8),\n",
    "    annotate_axes=False,\n",
    "    edges=False,\n",
    ")\n",
    "bounds = np.asarray([[-1.5, 1.5], [-1.5, 1.5], [-0.4, 0.4]]).T\n",
    "ax = fig.gca()\n",
    "ax.set_zticks(np.arange(-1, 1, 0.2))\n",
    "xlim, ylim, zlim = set_limit_and_aspect(fig, bounds, scale=1.1, decimals=None)\n",
    "ax.view_init(20, -150)\n",
    "im = fig2img(fig, dpi=400)\n",
    "im.save(load_path / \"components_012.png\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the same image again using tighter bounds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = make_latent_space_figure(\n",
    "    pca_codes,\n",
    "    points=additional_pca_codes,\n",
    "    start=start_pca_codes,\n",
    "    middle=middle_pca_codes,\n",
    "    end=end_pca_codes,\n",
    "    opacity=0.2,\n",
    "    s=2,\n",
    "    annotate_points=False,\n",
    "    figsize=(6, 6),\n",
    "    annotate_axes=False,\n",
    "    edges=False,\n",
    ")\n",
    "bounds = np.asarray([[-1, 1], [-1, 1], [-0.4, 0.4]]).T\n",
    "ax = fig.gca()\n",
    "# ax.set_zticks(np.arange(-1, 1, 0.2))\n",
    "xlim, ylim, zlim = set_limit_and_aspect(fig, bounds, scale=1.1)\n",
    "ax.view_init(40, -155)\n",
    "im = fig2img(fig, dpi=400)\n",
    "im.save(load_path / \"components_012_crop.png\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "pytorch3d-2023",
   "language": "python",
   "name": "pytorch3d-2023"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.16"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
