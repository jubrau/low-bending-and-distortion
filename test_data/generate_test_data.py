import argparse
import json
import pickle
from pathlib import Path

import torch
import numpy as np

from scripts.constants import MANIFOLDS

parser = argparse.ArgumentParser()
parser.add_argument(
    "-o",
    "--output",
    required=False,
    type=Path,
    help="Path to where to store test data",
    default="test_data",
)

parser.add_argument(
    "-s",
    "--savename",
    required=False,
    type=str,
    help="Filename of stored test data",
)

parser.add_argument(
    "-f",
    "--filename",
    required=True,
    type=Path,
    help="Path to arguments json file.",
)

parser.add_argument("-n", "--number", required=True, type=int, help="Number of samples")

parser.add_argument("--seed", required=False, type=int, default=42, help="Random seed")

cargs = parser.parse_args()
with open(cargs.filename, "r") as json_data:
    args = json.load(json_data)

# reproducibility
torch.manual_seed(cargs.seed)
torch.backends.cudnn.deterministic = True
np.random.seed(cargs.seed)
torch.backends.cudnn.benchmark = False

args_data = args["data"]
manifold_name = args_data["manifold"]
manifold = MANIFOLDS[manifold_name](**args_data.get("manifold_args", {}))

x = manifold.random_point(cargs.number)
y = manifold.random_point(cargs.number)
dist = manifold.dist(x, y)
interpolations = manifold.midpoint(x, y)

data = {"x": x, "y": y, "distances": dist, "interpolations": interpolations}

if cargs.savename == None:
    filename = f"{manifold_name}_max_{cargs.number}.pickle"
else:
    filename = cargs.savename

with open(cargs.output / filename, "wb") as f:
    pickle.dump(data, f)
