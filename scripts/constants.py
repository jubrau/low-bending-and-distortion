from lbad.data.manifold import Cylinder, Hemisphere
from lbad.data.manifold.klein_bottle import KleinBottleRenderer, KleinBottle
from lbad.data.manifold.special_orthogonal import SO3
from lbad.data.manifold.hemisphere import HemisphereProjectionRenderer, SundialRenderer
from lbad.data.manifold.cylinder import (
    CylinderAstroidRenderer,
    CylinderEllipseRendererCov,
    CylinderEllipseRenderer,
)
from lbad.data.manifold.special_orthogonal import CowRenderer

MANIFOLDS = {
    "cylinder": Cylinder,
    "hemisphere": Hemisphere,
    "special_orthogonal": SO3,
    "klein_bottle": KleinBottle,
}

RENDERER = {
    "cylinder_ellipse_cov": CylinderEllipseRendererCov,
    "cylinder_ellipse": CylinderEllipseRenderer,
    "cylinder_astroid": CylinderAstroidRenderer,
    "hemisphere_projection": HemisphereProjectionRenderer,
    "hemisphere_sundial": SundialRenderer,
    "cow_3d": CowRenderer,
    "klein_bottle_arc": KleinBottleRenderer,
}
