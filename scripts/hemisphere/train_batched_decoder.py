import argparse
import json
from pathlib import Path

from scripts.train import main

parser = argparse.ArgumentParser()
parser.add_argument(
        "-b",
        "--basepath",
        required=False,
        type=str,
        default="runs",
        help="Name of the base folder where experiments should be saved. "
        "Final path: basepath/savepath/experimentname"
        "Default: runs",
    )

cargs = parser.parse_args()
base_path = cargs.basepath
filepath = "scripts/hemisphere/args_train_decoder_sundial.json"

with open(filepath, "r") as json_data:
    args = json.load(json_data)

for seed in [42, 115, 866]:
    base_path = Path("runs")
    save_path_encoder = Path(f"interpolation/hemisphere/hemisphere_sundial/encoder/seed_{seed}")
    save_path_decoder = Path(f"interpolation/hemisphere/hemisphere_sundial/decoder/seed_{seed}")
    path = base_path / save_path_encoder
    args["train"]["seed"] = seed
    for child in path.iterdir():
        if child.is_dir():
            save_path_experiment = child.relative_to(base_path / save_path_encoder)
            args["train"]["load_dir_encoder"] = str(child)
            print("load_dir_encoder: " + str(child))
            if not Path.exists(base_path / save_path_decoder / save_path_experiment):
                main(args, savepath=save_path_decoder, basepath=base_path)
