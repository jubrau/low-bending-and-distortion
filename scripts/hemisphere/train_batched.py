import argparse
import json
from scripts.train import main

parser = argparse.ArgumentParser()
parser.add_argument(
        "-b",
        "--basepath",
        required=False,
        type=str,
        default="runs",
        help="Name of the base folder where experiments should be saved. "
        "Final path: basepath/savepath/experimentname"
        "Default: runs",
    )

cargs = parser.parse_args()
base_path = cargs.basepath

filepath = "scripts/hemisphere/args_train_encoder_sundial_f0.json"

with open(filepath, "r") as json_data:
    args = json.load(json_data)

for seed in [42, 115, 866]:
    for flatness_weight in [0, 10]:
        for eps_rel in [0.125, 0.25, 0.5, 1]:
            args["train"]["seed"] = seed
            args["train"]["flatness_weight"] = flatness_weight
            args["train"]["sampling_params"]["eps_rel"] = eps_rel
            print(
                f"Seed: {seed}    Flatness Weight: {flatness_weight}    eps: {eps_rel}"
            )
            if seed == 42 and flatness_weight == 0 and eps_rel == 0.125:
                continue
            else:
                main(
                    args,
                    savepath=f"interpolation/hemisphere/hemisphere_sundial/encoder/seed_{seed}",
                    basepath=base_path
                )
