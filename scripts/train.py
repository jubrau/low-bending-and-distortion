import importlib
import logging
from pathlib import Path

import numpy as np
import torch

from lbad.data.structures import PrecomputedDatasetWithTransform, MultiIndexDataLoader
from lbad.data.toystructures import ToyDataset, InfiniteDataLoader
from scripts.constants import MANIFOLDS, RENDERER

from lbad import train, train_decoder
from lbad.utils import to_torch_and_device

import json
import argparse


def main(
    args,
    savepath=None,
    basepath="runs",
    basepathload=None,
    experimentname=None,
    startepoch=0,
    device="cuda"
):
    args_train = args["train"]
    args_architecture = args.get("architecture", {})
    args_data = args.get("data", {})

    # reproducibility
    torch.manual_seed(args_train["seed"])
    torch.backends.cudnn.deterministic = True
    np.random.seed(args_train["seed"])
    torch.backends.cudnn.benchmark = False
    # torch.use_deterministic_algorithms(True)
    # some non-deterministic behavior seems to remain when using cuda

    # if only decoder, combine arguments
    only_decoder = args_train.get("load_dir_encoder", None) is not None

    basepath = Path(basepath)
    if basepathload is None:
        basepathload = basepath
    basepathload = Path(basepathload)

    if only_decoder:
        load_dir_encoder = basepathload / args_train["load_dir_encoder"]
        with open(load_dir_encoder / "args.json", "r") as json_data:
            args_encoder = json.load(json_data)

        args_data = args_encoder["data"]
        args_architecture = args_encoder["architecture"] | args_architecture
        args_architecture["include_decoder"] = True
        args = {
            "architecture": args_architecture,
            "train": args_train,
            "data": args_data,
            "test": args.get("test", args_encoder.get("test", None)),
        }

    if args_train.get("load_path_init_file", None) is not None:
        load_path_init_file = basepathload / args_train["load_path_init_file"]
    else:
        load_path_init_file = None

    manifold_name = args_data["manifold"]
    renderer_name = args_data["renderer"]

    args["architecture"]["width"] = args_data["renderer_args"]["im_width"]

    include_decoder = args_architecture.get("include_decoder", True)

    # savepath construction
    if savepath is None:
        if only_decoder:
            prefix = "decoder"
        elif include_decoder:
            prefix = "autoencoder"
        else:
            prefix = "encoder"
        savepath = "interpolation" / Path(manifold_name) / renderer_name / prefix

    manifold = MANIFOLDS[manifold_name](**args_data.get("manifold_args", {}))
    renderer = RENDERER[renderer_name](
        **args_data["renderer_args"], device=device
    )

    train_with_reg = not (
        args_train.get("weight_encoder_reg", 0) == 0
        and args_train.get("weight_decoder_reg", 0) == 0
    )

    sampling_strategy = args_train.get("sampling_strategy")
    if (sampling_strategy is None) or (not sampling_strategy.startswith("dataset")):
        dataset = ToyDataset(
            manifold,
            transform=renderer.render,
            sample_tuples=train_with_reg,
            sampling_strategy=args_train.get("sampling_strategy", None),
            device=device,
        )
    else:
        dataset_name = sampling_strategy.split(":")[1]
        DatasetClass = getattr(
            importlib.import_module("lbad.data.manifold"), dataset_name
        )
        dataset = DatasetClass(
            **args_data.get("manifold_args", {}),
            transform=renderer.render,
            sample_tuples=train_with_reg,
            device=device,
        )
    if args["test"]["data"] is not None:
        dataset_test = PrecomputedDatasetWithTransform(
            load_path=f"test_data/{args['test']['data']}",
            num_samples=args["test"].get("dataset_size", None),
            transform=lambda x: renderer.render(to_torch_and_device(x, device)),
        )
        dataloader_test = MultiIndexDataLoader(
            dataset_test, batch_size=args["test"].get("batch_size", 1024), shuffle=False
        )
        example_images = dataset_test.example_batch()
    else:
        dataloader_test = None
        example_images = None

    dataloader = InfiniteDataLoader(
        dataset,
        batch_size=args_train["batch_size"],
        dataset_size=args_train["dataset_size"],
        sampling_params=args_train.get("sampling_params", None),
    )

    if experimentname is None:
        reduced_size = args_architecture["reduced_size"]
        if only_decoder:
            experimentname = Path(args["train"]["load_dir_encoder"]).parts[-1]
            if (
                args_encoder["architecture"]["reduced_size"] is None
                and reduced_size is not None
            ):
                experimentname += f"_r{reduced_size}"
        else:
            args_reg = args_train
            if not train_with_reg and not only_decoder:
                if reduced_size is None:
                    experimentname = "noreg"
                else:
                    experimentname = f"noreg_r{reduced_size}"
            else:
                try:
                    eps_rel = args_reg["sampling_params"]["eps_rel"]
                    flatness_weight = args_reg["flatness_weight"]
                    sampling_strategy = dataset.sampling_strategy
                except KeyError:
                    raise ValueError(
                        "Cannot use default naming structure since not all necessary"
                        "parameters are present. Please specify a custom experiment "
                        "name via the option --experimentname / -e."
                    )
                if reduced_size is not None:
                    experimentname = (
                        f"e{eps_rel}_f{flatness_weight}_r{reduced_size}_"
                        f"{sampling_strategy}"
                    )
                else:
                    experimentname = (
                        f"e{eps_rel}_f{flatness_weight}_{sampling_strategy}"
                    )

    if savepath is not None:
        total_save_path = basepath / savepath / experimentname

        if total_save_path.exists():
            ans = input(
                "\nWARNING: Save path already exists, are you sure you want to "
                "continue? type y for yes: "
            )
            if ans != "y":
                quit()
    else:
        total_save_path = None

    if not only_decoder:
        train(
            dataloader,
            total_save_path,
            args=args,
            example_images=example_images,
            load_path_init_file=load_path_init_file,
            eval_every=1,
            image_every=10,
            device=device,
            dataloader_test=dataloader_test,
            start_epoch=startepoch,
        )
    else:
        train_decoder(
            dataloader,
            total_save_path,
            load_dir_encoder=load_dir_encoder,
            load_file_encoder=args_train["load_file_encoder"],
            args=args,
            example_images=example_images,
            eval_every=1,
            device=device,
            dataloader_test=dataloader_test,
            image_every=10,
            start_epoch=startepoch,
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f",
        "--filename",
        required=True,
        type=str,
        help="Path to arguments json file.",
    )
    parser.add_argument(
        "-s",
        "--savepath",
        required=False,
        type=str,
        help="Name of superfolder where experiment results should be saved."
        "Final path: basepath/savepath/experimentname"
        "Default: interpolation/manifold_name/renderer_name/prefix",
    )

    parser.add_argument(
        "-b",
        "--basepath",
        required=False,
        type=str,
        default="runs",
        help="Name of the base folder where experiments should be saved. "
        "Final path: basepath/savepath/experimentname"
        "Default: runs",
    )

    parser.add_argument(
        "--basepathload",
        required=False,
        type=str,
        default=None,
        help="Name of the base folder where specified weights are loaded from."
        "For example, when training the decoder, encoder weights are loaded from"
        "basepathload/args['train']['load_dir_encoder']",
    )

    parser.add_argument(
        "-e",
        "--experimentname",
        required=False,
        type=str,
        help="Name of sub-folder of savepath where experiment results should be saved."
        "Final path: basepath/savepath/experimentname"
        "Default: e{eps_rel}_f{flatness_weight}_r{reduced_size}_{sampling_strategy}",
    )

    parser.add_argument(
        "-d",
        "--device",
        required=False,
        type=str,
        default="cuda",
        help="Device on which to run the script"
    )

    parser.add_argument(
        "--startepoch",
        required=False,
        type=int,
        default=0,
        help="Start epoch, use to continue training.",
    )

    cargs = parser.parse_args()

    with open(cargs.filename, "r") as json_data:
        args = json.load(json_data)

    kwargs = vars(cargs)
    kwargs.pop("filename")
    logging.getLogger().setLevel(logging.INFO)
    main(args, **kwargs)
