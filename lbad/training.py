import json
import time
from collections import defaultdict
from typing import Iterable

import numpy as np
import torch
from torch import optim
from torch.utils.tensorboard import SummaryWriter

from lbad.loss import bending_distortion_loss, L2
from lbad.utils.evaluation import (
    eval_reconstruction,
    eval_bending_distortion_loss,
    eval_pca,
)
from lbad.utils import batched_forward_tuples
from lbad.utils.training import (
    WeightClipper,
    status,
    setup_networks,
    EarlyStopping,
)

from pathlib import Path


def train(
    dataloader,
    save_path,
    args=None,
    example_images=None,
    load_path_init_file=None,
    load_path=None,
    load_file_name=None,
    start_epoch=0,
    end_epoch=None,
    device=None,
    dataloader_test=None,
    image_every=10,
    eval_every=10,
    save_every=None,
    reference_loss=None,
):
    """
    Train an autoencoder with data from dataloader.

    Parameters
    ----------
    dataloader: Dataloader
        dataloader for dataset returning triples
    save_path: string or Path
        Path to folder where results are saved. The following objects are saved:
        - all custom code in a sub-folder /code
        - the args dictionary
        - network and optimizer state dicts every "save_every" epochs, suffix epoch
        - network and optimizer state dict after final epoch, suffix epoch
        - if reference loss is not None:
            network and optimizer state dicts after reference loss has been reached,
            suffix "final"
        - network and optimizer state dict after epoch where best low bending and low
        distortion loss was reached on test data, suffix "best"
        - json containing the best epoch
    args: dictionary of args, optional, default: None, use args from load_path
        The args dictionary is split into two sub-dictionaries, "train" and
        "architecture".
        The "train" dictionary uses the following parameters:
            epochs:
            device:
            seed: optional, default: 42
            weight_encoder_reg: optional, default: 0
            weight_decoder_reg: optional, default: 0
            num_components: optional, default: 0
            early stopping: dict, optional, default: None
                dictionary with the following parameters
                    quantity: 'train_reg_loss', 'test_reg_loss'
                        determine which quantity to use for early stopping
                    patience: see EarlyStopping
                    min_delta: see EarlyStopping
            lr:
            weight_decay:
            weight_constraints:
            flatness_weight: optional, default: 0
            isometry_weight: optional, default: 1
            isometry_penalty_parameter: optional, default: 0
        The "architecture" dictionary uses the following parameters:
            include_decoder: optional, default: True
            ...
    example_images: torch.Tensor
        tensor of example images for monitoring training progress
    load_path: string or Path
        path from where to load weights for the network
        optional, default None
    load_file_name: string
        name of weights file to load, optional, default: constructed from start_epoch
    start_epoch: int
        start epoch, optional, default: 0, use to continue training
    end_epoch: int
        end epoch, after how many epochs training should end. For example, setting
        start_epoch=100 and end_epoch=200
        would run 100 training loops
    device: string
        device on which to run the training, e.g. 'cuda:0'
    dataloader_test: Dataloader
        dataloader for test dataset returning triples, used for evaluation
    image_every: int
        if epoch is a multiple of image_every, a status image from the example images is
        generated in tensorboard
    eval_every: int
        if epoch is a multiple of eval_every, metrics are recorded in tensorboard
    save_every: int
        if epoch is a multiple of save_every, network and optimizer state dicts are
        saved
        if save_every = 0, no intermediate saving takes place
        optional, default: number of epochs // 5
    reference_loss: float
        reference loss for reconstruction loss on test data

    Returns
    -------
    network: Module
        trained network

    """

    if load_path_init_file is not None:
        state_dict = torch.load(load_path_init_file)
        with open(Path(load_path_init_file).parent / "args.json") as f:
            args_pretrained = json.load(f)
        # replace loaded args by given args
        args_pretrained.update(args)
        args = args_pretrained

    # case: continued training
    if load_path is not None or start_epoch > 0:
        if load_path is None:
            load_path = save_path
        load_path = Path(load_path)
        if args is None:
            with open(load_path / "args.json", "r") as f:
                args = json.load(f)
        if end_epoch is not None:
            args["train"]["epochs"] = end_epoch

    if save_path is not None:
        save_path = Path(save_path)
        Path.mkdir(save_path, parents=True, exist_ok=True)

        # saving
        with open(save_path / "args.json", "w") as f:
            json.dump(args, f, indent=4)

    # process parameters
    if save_every is None:
        save_every = args["train"]["epochs"] // 5
    include_decoder = args["architecture"].get("include_decoder", True)
    weight_encoder_reg = args["train"].get("weight_encoder_reg", 0)
    weight_decoder_reg = args["train"].get("weight_decoder_reg", 0)
    num_components = args["train"].get("num_components", None)
    lambda_hess = args["train"].get("flatness_weight", 0)
    lambda_grad = args["train"].get("isometry_weight", 1)
    c = args["train"].get("isometry_penalty_parameter", 0)
    if end_epoch is None:
        end_epoch = args["train"]["epochs"]

    if reference_loss is None:
        reference_loss = args["train"].get("reference_loss", None)

    if not include_decoder:
        save_name = "_encoder"
    else:
        save_name = ""

    early_stopping = args["train"].get("early_stopping", None)

    if early_stopping is not None:
        early_stopping = EarlyStopping(
            patience=early_stopping["patience"], min_delta=early_stopping["min_delta"]
        )
        if eval_every != 1:
            raise ValueError("Should evaluate every epoch for early stopping.")

    # setup network and training
    if include_decoder:
        encoder, decoder, autoencoder = setup_networks(
            args["architecture"], include_decoder=True
        )
        network = autoencoder
    else:
        encoder = setup_networks(args["architecture"], include_decoder=False)
        network = encoder

    network = network.to(device)
    parameters = network.parameters()
    optimizer = optim.Adam(
        parameters, lr=args["train"]["lr"], weight_decay=args["train"]["weight_decay"]
    )

    if args["train"].get("weight_constraints") is not None:
        clipper = WeightClipper(
            encoder,
            bound=args["train"]["weight_constraints"][1],
            bounds_per_layer={"conv_0": args["train"]["weight_constraints"][0]},
        )
        clip = True
    else:
        clip = False

    # loading for continued training
    if start_epoch > 0:
        if load_file_name is None:
            load_file_name = f"state_dict{save_name}_{start_epoch}.pth"
        state_dict = torch.load(load_path / load_file_name)
        try:
            optimizer_state_dict = torch.load(load_path / f"optimizer_{load_file_name}")
        except FileNotFoundError as e:
            optimizer_state_dict = None
            print(e)
        try:
            network.load_state_dict(state_dict)
        except RuntimeError:
            encoder.load_state_dict(state_dict)
        if optimizer_state_dict is not None:
            optimizer.load_state_dict(optimizer_state_dict)

    if load_path_init_file is not None:
        network.load_state_dict(state_dict)

    # setup monitoring
    writer = SummaryWriter(save_path)
    min_loss = np.inf
    last_time = time.time()
    best_epoch = -1

    # loss function
    loss_fun = bending_distortion_loss(
        lambda_hess=lambda_hess, lambda_grad=lambda_grad, sum_up=True, c=c
    )

    for epoch in range(start_epoch, end_epoch):
        network.train()
        epoch_losses = defaultdict(lambda: 0)
        num_samples = 0

        # console output
        if time.time() - last_time >= 60 or epoch == 0:
            print(f'{epoch}/{args["train"]["epochs"]}')
            last_time = time.time()

        for sample in dataloader:
            if dataloader.dataset.sample_tuples:
                x, y = sample["x"], sample["y"]
            else:
                x = sample
            num_samples += x.shape[0]

            # compute regularization loss
            if weight_encoder_reg > 0 or weight_decoder_reg > 0:
                x_code = encoder(x)
                y_code = encoder(y)
                code_of_interpolation = encoder(sample["interpolations"])
                interpolated_code = torch.lerp(x_code, y_code, 0.5)
                if weight_decoder_reg > 0:
                    pred_interpolation = decoder(interpolated_code)
                    reg_loss_decoder = L2(pred_interpolation - sample["interpolations"])
                if weight_encoder_reg > 0:
                    reg_loss_encoder = loss_fun(
                        interpolated_code,
                        code_of_interpolation,
                        sample["distances"],
                        x_code,
                        y_code,
                    )

            # compute standard reconstruction loss
            if include_decoder:
                out = autoencoder(x)
                loss_reconstruction = L2(out - x)

                if weight_encoder_reg > 0:
                    out2 = autoencoder(y)
                    loss_reconstruction = (loss_reconstruction + L2(out2 - y)) / 2

            # total loss
            loss_ae = 0
            if include_decoder:
                loss_ae = loss_ae + loss_reconstruction
            if weight_encoder_reg > 0:
                loss_ae = loss_ae + weight_encoder_reg * (
                    reg_loss_encoder["latent_total"]
                )
            if weight_decoder_reg > 0:
                loss_ae = loss_ae + weight_decoder_reg * reg_loss_decoder

            # check that loss is finite, otherwise stop iteration
            if not torch.isfinite(loss_ae):
                print("Terminating due to infinite loss.")
                break

            # optimization step
            optimizer.zero_grad()
            loss_ae.backward(retain_graph=False)
            optimizer.step()
            if clip:
                clipper.apply()

            # store losses for monitoring
            epoch_losses["total"] += loss_ae.item()
            if include_decoder:
                epoch_losses["rec"] += loss_reconstruction.item()
            if weight_encoder_reg > 0:
                for key, value in reg_loss_encoder.items():
                    epoch_losses[key] += value.item()
                epoch_losses["latent_total_w"] = (
                    epoch_losses["latent_total"] * weight_encoder_reg
                )
            if weight_decoder_reg > 0:
                epoch_losses["reg_decoder"] += reg_loss_decoder.item()
                epoch_losses["reg_decoder_w"] += (
                    reg_loss_decoder.item() * weight_decoder_reg
                )

        for key, value in epoch_losses.items():
            writer.add_scalar(key, value / num_samples, global_step=epoch)

        # monitoring on test data
        if epoch % eval_every == 0:
            network.eval()
            with torch.no_grad():
                # test data
                if dataloader_test is not None:
                    if weight_encoder_reg:
                        test_loss_latent = eval_bending_distortion_loss(
                            dataloader_test,
                            encoder,
                            lambda_hess=lambda_hess,
                            lambda_grad=lambda_grad,
                            c=c,
                            sum_up=True,
                        )

                        for key, value in test_loss_latent.items():
                            writer.add_scalar(key + "_test", value, global_step=epoch)

                    if include_decoder:
                        test_loss_rec = eval_reconstruction(
                            dataloader_test, autoencoder
                        )
                        writer.add_scalar("rec_test", test_loss_rec, global_step=epoch)

                    # pca
                    if num_components is not None:
                        latent_codes = batched_forward_tuples(
                            dataloader_test, encoder, used_images=("x",)
                        )["x"]
                        exp_var = eval_pca(latent_codes, num_components=num_components)
                        if isinstance(num_components, Iterable):
                            for i, n in enumerate(num_components):
                                writer.add_scalar(
                                    f"pca_exp_var_{n}", exp_var[i], global_step=epoch
                                )
                        else:
                            writer.add_scalar(
                                f"pca_exp_var_{num_components}",
                                exp_var,
                                global_step=epoch,
                            )

        # image visualization
        if (
            example_images is not None
            and image_every is not None
            and include_decoder
            and epoch % image_every == 0
        ):
            if args["architecture"]["colors"] == 1:
                data_formats = "HW"
            else:
                data_formats = "HWC"
            writer.add_image(
                "status_train",
                status(example_images, autoencoder, num_cols=6),
                dataformats=data_formats,
                global_step=epoch,
            )

        # saving weights
        if save_path is not None:
            if save_every > 0 and epoch % save_every == 0:
                torch.save(
                    network.state_dict(),
                    f=save_path / f"state_dict{save_name}_{epoch}.pth",
                )
                torch.save(
                    optimizer.state_dict(),
                    save_path / f"optimizer_state_dict{save_name}_{epoch}.pth",
                )
            if weight_encoder_reg > 0 and dataloader_test is not None:
                if test_loss_latent["latent_total"] < min_loss:
                    torch.save(
                        network.state_dict(),
                        save_path / f"state_dict{save_name}_best.pth",
                    )
                    torch.save(
                        optimizer.state_dict(),
                        save_path / f"optimizer{save_name}_state_dict_best.pth",
                    )
                    min_loss = test_loss_latent["latent_total"]
                    best_epoch = epoch
            if reference_loss is not None and test_loss_rec <= reference_loss:
                torch.save(
                    network.state_dict(), save_path / f"state_dict{save_name}_final.pth"
                )
                torch.save(
                    optimizer.state_dict(),
                    save_path / f"optimizer{save_name}_state_dict_final.pth",
                )
                break
            if early_stopping is not None:
                stopping_quantity = args["train"]["early_stopping"]["quantity"]
                if stopping_quantity == "test_reg_loss":
                    early_stopping(test_loss_latent["latent_total"])
                elif stopping_quantity == "train_reg_loss":
                    early_stopping(epoch_losses["latent_total"])
                else:
                    raise ValueError(
                        f'Unknown stopping quantity "{stopping_quantity}".'
                    )
                if early_stopping.counter == 0:
                    # quantity has improved
                    torch.save(
                        network.state_dict(),
                        save_path / f"state_dict{save_name}_early_stop_best.pth",
                    )
                    torch.save(
                        optimizer.state_dict(),
                        save_path
                        / f"optimizer{save_name}_state_dict_early_stop_best.pth",
                    )
                if early_stopping.early_stop:
                    torch.save(
                        network.state_dict(),
                        save_path / f"state_dict{save_name}_early_stop_end.pth",
                    )
                    torch.save(
                        optimizer.state_dict(),
                        save_path
                        / f"optimizer{save_name}_state_dict_early_stop_end.pth",
                    )
                    break

    if save_path is not None:
        with open(save_path / "info.json", "w") as f:
            json.dump({"best epoch": best_epoch, "final epoch": epoch}, f, indent=4)

        torch.save(
            network.state_dict(), f=save_path / f"state_dict{save_name}_{epoch}.pth"
        )
        torch.save(
            optimizer.state_dict(),
            save_path / f"optimizer_state_dict{save_name}_{epoch}.pth",
        )

    return network
