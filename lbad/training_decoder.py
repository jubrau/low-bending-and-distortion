import json
import time
from copy import deepcopy
from pathlib import Path

import numpy as np
import torch
from torch import optim
from torch.utils.tensorboard import SummaryWriter

from lbad.loss import L2
from lbad.utils.evaluation import eval_reconstruction
from lbad.utils.training import EarlyStopping, setup_networks, status, init_pca


def train_decoder(
    dataloader,
    save_path,
    load_dir_encoder=None,
    load_file_encoder=None,
    load_path_autoencoder_init_file=None,
    args=None,
    example_images=None,
    load_path=None,
    load_file_name=None,
    start_epoch=0,
    end_epoch=None,
    device=None,
    dataloader_test=None,
    image_every=10,
    eval_every=10,
    save_every=None,
):
    """
    Train a decoder with a pretrained encoder with data from dataloader.

    Parameters
    ----------
    dataloader: Dataloader
        dataloader for dataset returning single elements
    save_path: string or Path
        Path to folder where results are saved. The following objects are saved:
        - all custom code in a sub-folder /code
        - the args dictionary
        - network and optimizer state dicts every "save_every" epochs, suffix epoch
        - network and optimizer state dict after final epoch, suffix epoch
        - if reference loss is not None:
            network and optimizer state dicts after reference loss has been reached,
            suffix "final"
        - network and optimizer state dict after epoch where the best reconstruction
            loss was reached on test data, suffix "best"
        - json containing the best epoch
    load_dir_encoder: string or Path
        path from where to load encoder weights
        weights may be for "full" encoder, then init_pca is used
    load_file_encoder: string
        name of weights file to load,
    load_path_autoencoder_init_file: string of Path, optional
        path to autoencoder init file, weights may be for "full" autoencoder,
        then init_pca is used
    args: dictionary of args, optional, default: None, use args from load_path
        The args dictionary is split into two sub-dictionaries, "train" and
        "architecture".
        The "train" dictionary uses the following parameters:
            epochs:
            device:
            seed: optional, default: 42
            early stopping: optional, default: 0
            lr:
            weight_decay:
            reference_loss: float
                reference loss for reconstruction loss on test data
        The "architecture" dictionary uses the following parameters:
            ...
    example_images: torch.Tensor
        tensor of example images for monitoring training progress
    load_path: string or Path
        path from where to load weights for the network
        optional, default None
    load_file_name: string
        name of weights file to load, optional, default: constructed from start_epoch
    start_epoch: int
        start epoch, optional, default: 0, use to continue training
    end_epoch: int
        end epoch, after how many epochs training should end. For example, setting
        start_epoch=100 and end_epoch=200 would run 100 training loops
    device: string
        device on which to run the training, e.g. 'cuda:0'
    dataloader_test: Dataloader
        dataloader for test dataset returning triples, used for evaluation
    image_every: int
        if epoch is a multiple of image_every, a status image from the example images is
        generated in tensorboard
    eval_every: int
        if epoch is a multiple of eval_every, metrics are recorded in tensorboard
    save_every: int
        if epoch is a multiple of save_every, network and optimizer state dicts are
        saved
        if save_every = 0, no intermediate saving takes place
        optional, default: number of epochs // 5

    Returns
    -------
    network: Module
        trained network

    """
    if load_dir_encoder is not None and load_path_autoencoder_init_file is not None:
        print(
            "WARNING: Both load_dir_encoder and load_path_decoder_init_file have "
            "been set. Ignoring load_path_decoder_init_file."
        )

    # load encoder architecture args
    if load_dir_encoder is not None:
        load_dir_encoder = Path(load_dir_encoder)
        state_dict_encoder = torch.load(load_dir_encoder / load_file_encoder)
    else:
        state_dict = torch.load(load_path_autoencoder_init_file)
        load_path_autoencoder_init_file = Path(load_path_autoencoder_init_file)

    # case: continued training
    if load_path is not None or start_epoch > 0:
        if load_path is None:
            load_path = save_path
        load_path = Path(load_path)
        if args is None:
            with open(load_path / "args.json", "r") as f:
                args = json.load(f)
        args["train"]["epochs"] = end_epoch

    if save_path is not None:
        save_path = Path(save_path)
        Path.mkdir(save_path, parents=True, exist_ok=True)

        # saving
        with open(save_path / "args.json", "w") as f:
            json.dump(args, f, indent=4)

    # process parameters
    if save_every is None:
        save_every = args["train"]["epochs"] // 5
    if end_epoch is None:
        end_epoch = args["train"]["epochs"]

    early_stopping = args["train"].get("early_stopping", None)
    reference_loss = args["train"].get("reference_loss", None)

    if early_stopping is not None:
        early_stopping = EarlyStopping(
            patience=early_stopping["patience"], min_delta=early_stopping["min_delta"]
        )
        if eval_every != 1:
            raise ValueError("Should evaluate every epoch for early stopping.")

    # setup network and training
    encoder, decoder, autoencoder = setup_networks(
        args["architecture"],
        include_decoder=True,
        additional_layers=args["architecture"].get("additional_layers", None),
    )
    autoencoder = autoencoder.to(device)
    parameters = decoder.parameters()
    optimizer = optim.Adam(
        parameters, lr=args["train"]["lr"], weight_decay=args["train"]["weight_decay"]
    )

    # load weights
    if load_dir_encoder is not None:
        # load encoder weights
        try:
            encoder.load_state_dict(state_dict_encoder)
        except RuntimeError:
            # if weights don't fit, we need to add reduction
            # first, build full encoder to load weights
            args_temp = deepcopy(args)
            args_temp["architecture"]["reduced_size"] = None
            encoder_full = setup_networks(
                args_temp["architecture"], include_decoder=False
            )
            encoder_full.load_state_dict(state_dict_encoder)
            encoder_full.to(device)
            reduced_size = args["architecture"]["reduced_size"]
            dataloader_test.dataset.sample_tuples = False
            # initialize final reduction layers by pca
            state_dict_encoder = init_pca(
                encoder_full, dataloader_test, reduced_size=reduced_size
            )
            dataloader_test.dataset.sample_tuples = True
            encoder.load_state_dict(state_dict_encoder)
    elif load_path_autoencoder_init_file is not None:
        # load init decoder weights
        try:
            autoencoder.load_state_dict(state_dict)
        except RuntimeError:
            # setup full autoencoder
            args_temp = deepcopy(args)
            args_temp["architecture"]["reduced_size"] = None
            encoder_full, decoder_full, autoencoder_full = setup_networks(
                args_temp["architecture"], include_decoder=True
            )
            autoencoder_full.load_state_dict(state_dict)
            autoencoder_full.to(device)
            reduced_size = args["architecture"]["reduced_size"]
            dataloader_test.dataset.sample_tuples = False
            state_dict, _, _ = init_pca(
                encoder_full,
                dataloader_test,
                reduced_size=reduced_size,
                decoder_state_dict=decoder_full.state_dict(),
            )
            dataloader_test.dataset.sample_tuples = True
            autoencoder.load_state_dict(state_dict)

    # loading for continued training
    if start_epoch > 0:
        if load_file_name is None:
            load_file_name = f"state_dict_{start_epoch}.pth"
        state_dict = torch.load(load_path / load_file_name)
        try:
            optimizer_state_dict = torch.load(load_path / f"optimizer_{load_file_name}")
        except FileNotFoundError as e:
            optimizer_state_dict = None
            print(e)
        try:
            autoencoder.load_state_dict(state_dict)
        except RuntimeError:
            encoder.load_state_dict(state_dict)
        if optimizer_state_dict is not None:
            optimizer.load_state_dict(optimizer_state_dict)

    # setup monitoring
    writer = SummaryWriter(save_path)
    min_loss = np.inf
    last_time = time.time()
    best_epoch = -1

    for epoch in range(start_epoch, end_epoch):
        autoencoder.train()

        loss_rec = 0
        num_samples = 0

        # console output
        if time.time() - last_time >= 60 or epoch == 0:
            print(f'{epoch}/{args["train"]["epochs"]}')
            last_time = time.time()

        for x in dataloader:
            num_samples += x.shape[0]

            # compute standard reconstruction loss
            out = autoencoder(x)
            loss_ae = L2(out - x)

            # check that loss is finite, otherwise stop iteration
            if not torch.isfinite(loss_ae):
                print("Terminating due to infinite loss.")
                break

            # optimization step
            optimizer.zero_grad()
            loss_ae.backward(retain_graph=False)
            optimizer.step()

            # store losses for monitoring
            loss_rec += loss_ae.item()

        writer.add_scalar("rec", loss_rec / num_samples, global_step=epoch)

        # monitoring on test data
        if epoch % eval_every == 0:
            autoencoder.eval()
            with torch.no_grad():
                # test data
                if dataloader_test is not None:
                    test_loss_rec = eval_reconstruction(dataloader_test, autoencoder)
                    writer.add_scalar("rec_test", test_loss_rec, global_step=epoch)

        # image visualization
        if (
            example_images is not None
            and image_every is not None
            and epoch % image_every == 0
        ):
            if args["architecture"]["colors"] == 1:
                data_formats = "HW"
            else:
                data_formats = "HWC"
            writer.add_image(
                "status_train",
                status(example_images, autoencoder, num_cols=6),
                dataformats=data_formats,
                global_step=epoch,
            )

        # saving weights
        if save_path is not None:
            if save_every > 0 and epoch % save_every == 0:
                torch.save(
                    autoencoder.state_dict(), f=save_path / f"state_dict_{epoch}.pth"
                )
                torch.save(
                    optimizer.state_dict(),
                    save_path / f"optimizer_state_dict_{epoch}.pth",
                )
            if dataloader_test is not None:
                if test_loss_rec < min_loss:
                    torch.save(
                        autoencoder.state_dict(), save_path / "state_dict_best.pth"
                    )
                    torch.save(
                        optimizer.state_dict(),
                        save_path / "optimizer_state_dict_best.pth",
                    )
                    min_loss = test_loss_rec
                    best_epoch = epoch
            if reference_loss is not None and test_loss_rec <= reference_loss:
                torch.save(autoencoder.state_dict(), save_path / "state_dict_final.pth")
                torch.save(
                    optimizer.state_dict(),
                    save_path / "optimizer_state_dict_final.pth",
                )
                break
            if early_stopping is not None:
                if args["train"]["early_stopping"]["loss"] == "test":
                    early_stopping(test_loss_rec)
                else:
                    early_stopping(loss_rec)
                if early_stopping.early_stop:
                    torch.save(
                        autoencoder.state_dict(),
                        save_path / "state_dict_early_stop.pth",
                    )
                    torch.save(
                        optimizer.state_dict(),
                        save_path / "optimizer_state_dict_early_stop.pth",
                    )
                    break

    with open(save_path / "info.json", "w") as f:
        json.dump({"best epoch": best_epoch, "final epoch": epoch}, f, indent=4)

    torch.save(autoencoder.state_dict(), f=save_path / f"state_dict_{epoch}.pth")
    torch.save(optimizer.state_dict(), save_path / f"optimizer_state_dict_{epoch}.pth")

    return autoencoder
