import logging
import math
import shutil
import sys
from pathlib import Path

import numpy as np
import torch
from sklearn.decomposition import PCA
from torch.nn import LeakyReLU

from lbad.models import LeakySoftplus
from lbad.models.autoencoder import Encoder, Decoder, Autoencoder
from lbad.utils import batched_forward, to_torch_and_device
from lbad.utils.plotting import make_mosaic


class WeightClipper:
    """
    An instance of this class can be used as a function which clamps network weights to
    the range [-bound, bound].
    Layer-specific bounds can be specified by the bounds_per_layer dictionary.
    Usage: net.apply(clipper)
    """

    def __init__(self, module, bound=5.0, bounds_per_layer=None):
        self.bound = bound
        if bounds_per_layer is None:
            bounds_per_layer = {}
        bounds_per_layer_temp = bounds_per_layer
        self.bounds_per_layer = {}
        self.module = module
        for name, m in module.named_modules():
            # apply constraint to weights
            if hasattr(m, "weight"):
                bound = None
                for layer, bd in bounds_per_layer_temp.items():
                    if name.startswith(layer):
                        bound = bd
                        break
                if bound is None:
                    bound = self.bound
                self.bounds_per_layer[name] = bound

    def apply(self):
        for name, m in self.module.named_modules():
            bound = self.bounds_per_layer.get(name, None)
            if bound is not None:
                m.weight.data = m.weight.data.clamp(-bound, bound)


def status(x, autoencoder, num_cols=8):
    """
    Make some images that show the progress of training (sampled from latent space,
    interpolations, reconstructions)
    :param x: list of example images to work on, should have at least num_cols² entries.
    :param num_cols: number of columns in mosaic image
    :return: mosaic of mosaics, numpy array of unsigned ints between 0 and 255
    """
    encoder = autoencoder.encoder
    decoder = autoencoder.decoder
    z = encoder(x).detach()

    # reconstruction mosaic
    reconstruction = autoencoder(x[: num_cols**2])
    reconstructions_mosaic = make_mosaic(
        np.moveaxis(reconstruction.detach().cpu().numpy(), 1, -1), border=1
    )

    # interpolation mosaic
    a, b = z[:num_cols], z[num_cols : 2 * num_cols]  # split batch
    # list of length num_cols-2=num_alphas (different interpolation values),
    # each element has shape (num_cols, c, h, w) (num_cols = different images)
    x_interp = [
        autoencoder.interpolate(a, b, t).cpu().detach().numpy()
        for t in np.linspace(0, 1, num_cols)[1:-1]
    ]
    x_interp = np.stack(x_interp, axis=1)  # (num_cols, num_alphas=num_cols-2, c, h, w)
    x_interp = np.moveaxis(x_interp, 2, -1)  # (num_cols, num_cols-2, h, w, c)

    x_fixed = np.moveaxis(x.detach().cpu().numpy(), 1, -1)  # (num_cols, h, w, c)
    x_fixed = np.expand_dims(x_fixed, 1)  # (num_cols, 1, h, w, c)
    images = np.concatenate(
        [x_fixed[:num_cols], x_interp, x_fixed[num_cols : 2 * num_cols]], axis=1
    )
    images = np.vstack(list(images))
    interpolation_mosaic = make_mosaic(images, border=1)

    # sample mosaic
    z_sample = torch.normal(
        z.mean(axis=0, keepdim=True).expand(z.shape),
        z.std(axis=0, keepdim=True).expand(z.shape),
    )
    z_sample = z_sample[: num_cols**2]
    x_sample = decoder(z_sample)
    x_sample = np.moveaxis(x_sample.data.cpu().numpy(), 1, -1)
    sample_mosaic = make_mosaic(x_sample, border=1)

    chunks = [reconstructions_mosaic, interpolation_mosaic, sample_mosaic]
    chunks = [
        np.pad(e, [(0, 4), (0, 4), (0, 0)], mode="constant", constant_values=1)
        for e in chunks
    ]
    return np.uint8(np.clip(make_mosaic(chunks) * 255, 0, 255)).squeeze()


def setup_networks(args, include_decoder=True, additional_layers=None):
    """

    Parameters
    ----------
    args
    include_decoder
    additional_layers

    Returns
    -------
    encoder, decoder, autoencoder, if include_decoder=True
    encoder, else
    """
    # todo update documentation
    # if args.get('include_decoder', None) is not None:
    #     include_decoder = args['include_decoder']
    num_blocks = args.get("num_blocks", None)
    if num_blocks is None:
        num_blocks = int(round(math.log(args["width"] // args["latent_width"], 2)))

    if args.get("reduced_size", None) is not None:
        latent_size = args["latent_width"] ** 2 * args["latent"]
        reduced_size = (latent_size, args["reduced_size"])
    else:
        reduced_size = None

    if args.get("activation_approximation") is not None:

        def activation():
            return LeakySoftplus(beta=args["activation_approximation"])

    else:
        activation = LeakyReLU

    encoder = Encoder(
        num_blocks,
        args["depth"],
        args["latent"],
        args["colors"],
        batch_norm=args.get("batch_norm", False),
        reduce=reduced_size,
        activation=activation,
        pool=args.get("pool", True),
        pad=args.get("pad", 1),
    )
    if include_decoder:
        # additional_layers = args.get('additional_layers', None)
        reduced_size = args.get("reduced_size", None)
        if reduced_size is not None:
            if additional_layers is not None:
                reduced_size = (
                    (args["reduced_size"],) + additional_layers + (latent_size,)
                )
            else:
                reduced_size = (args["reduced_size"], latent_size)
        decoder = Decoder(
            num_blocks,
            args["depth"],
            args["latent"],
            args["colors"],
            batch_norm=args.get("batch_norm", False),
            reduce=reduced_size,
            activation=activation,
            upsample=args.get("pool", True),
        )
        autoencoder = Autoencoder(encoder, decoder)
        return encoder, decoder, autoencoder
    else:
        return encoder


class EarlyStopping:
    # https://debuggercafe.com/using-learning-rate-scheduler-and-early-stopping-with-pytorch/ # noqa
    """
    Early stopping to stop the training when the loss does not improve after
    certain epochs.
    """

    def __init__(self, patience=5, min_delta=0):
        """Initialize.

        Parameters
        ----------
        patience: int, optional, default: 5
            how many epochs to wait before stopping when loss is not improving
        min_delta: minimum difference between new loss and old loss for new loss to be
        considered as an improvement
        """
        self.patience = patience
        self.min_delta = min_delta
        self.counter = 0
        self.best_loss = None
        self.early_stop = False

    def __call__(self, val_loss):
        if self.best_loss is None:
            self.best_loss = val_loss
        elif self.best_loss - val_loss > self.min_delta:
            self.best_loss = val_loss
            # reset counter if validation loss improves
            self.counter = 0
        elif self.best_loss - val_loss < self.min_delta:
            self.counter += 1
            logging.info(f"Early stopping counter {self.counter} of {self.patience}")
            if self.counter >= self.patience:
                logging.info("Early stopping")
                self.early_stop = True


def store_code(save_path, proj_dir):
    """Store relevant project code.

    Parameters
    ----------
    save_path: Path or string
        path where to store the code
    proj_dir: Path or string
        path to directory where project code is stored
    """
    if save_path is not None:
        Path.mkdir(save_path, parents=True, exist_ok=True)

        loaded_files = []
        for module in sys.modules.values():
            try:
                module_file = module.__dict__["__file__"]
                if module_file is not None:
                    loaded_files.append(module_file)
            except KeyError:
                continue

        code_files = [str(path) for path in (Path(proj_dir).rglob("*.py"))]
        relevant_files = list(set(code_files).intersection(set(loaded_files)))

        for file in relevant_files:
            path = Path(save_path) / Path(file).relative_to(proj_dir)
            Path.mkdir(path.parent, parents=True, exist_ok=True)
            shutil.copy(file, path)


def init_pca(encoder, dataloader, reduced_size, decoder_state_dict=None):
    """Initialize state dict for reduced network from previously trained encoder /
    decoder using PCA.

    Parameters
    ----------
    encoder: Encoder
        pretrained encoder network
    dataloader: Dataloader
        dataloader used for computing PCA
    reduced_size: int
        size to which latent representation should be reduced
    decoder_state_dict: dict, optional
        pretrained decoder weights

    Returns
    -------
    if decoder is not None:
    state_dict, encoder_state_dict, decoder_state_dict: dicts
        state dicts which can be used to initialize autoencoder / encoder / decoder
        which have an additional "linear_reduce" / "linear_upscale" layer
    else:
    encoder_state_dict

    """
    latent_codes = batched_forward(dataloader, encoder)
    pca = PCA(n_components=reduced_size)
    pca.fit(latent_codes)
    encoder_state_dict = encoder.state_dict()
    encoder_state_dict["linear_reduce.1.weight"] = to_torch_and_device(pca.components_)
    encoder_state_dict["linear_reduce.1.bias"] = to_torch_and_device(
        -np.matmul(pca.components_, pca.mean_)
    )
    if decoder_state_dict is not None:
        decoder_state_dict["linear_upscale.0.weight"] = to_torch_and_device(
            pca.components_.T
        )
        decoder_state_dict["linear_upscale.0.bias"] = to_torch_and_device(pca.mean_)
        state_dict = {}
        for key, value in encoder_state_dict.items():
            state_dict[f"encoder.{key}"] = value
        for key, value in decoder_state_dict.items():
            state_dict[f"decoder.{key}"] = value
        return state_dict, encoder_state_dict, decoder_state_dict
    else:
        return encoder_state_dict
