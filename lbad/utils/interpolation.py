import numpy as np


def interpolate_along_path(func):
    """
    Take a function which can interpolate between two objects and return a function that
    makes an interpolation path.

    Parameters
    ----------
    func: function taking three positional arguments (x, y, alpha) and optional keyword
        arguments function that interpolates between x and y with weight alpha

    Returns
    -------
    func: function taking two positional arguments (x, y) and num with default 10
        function that generates num additional interpolation points between x and y,
        returns also x and y
    """

    def inner(x, y, num=10, **params):
        alphas = np.linspace(0, 1, num + 2)[1:-1]
        try:
            return (
                [x] + [func(x, y, alpha, **params).detach() for alpha in alphas] + [y]
            )
        except AttributeError:
            return [x] + [func(x, y, alpha, **params) for alpha in alphas] + [y]

    return inner
