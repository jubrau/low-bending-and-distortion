import numpy as np
import torch


def uniform_ball(
    num_samples: int, dim: int, radius: float = 1.0, device="cpu", radius_min: float = 0
) -> torch.Tensor:
    """Sample points uniformly in a ball.

    Parameters
    ----------
    num_samples: int
        number of samples to generate
    dim: int
        dimension of ambient space
    radius: float, default: 1
        radius of ball
    radius_min: float, default: 0
        minimal radius of points (i.e. hole in the middle)
    device: string
        device on which the returned tensor should be located

    Returns
    -------
    x: np.ndarray of shape [num_samples, dim]
        uniformly distributed points in ball

    Notes
    -----
    http://extremelearning.com.au/how-to-generate-uniformly-random-points-on-n-spheres-and-n-balls/
    """
    if radius_min > radius:
        raise ValueError(
            f"radius_min={radius_min:.2f} greater than radius={radius:.2f}."
        )

    s = uniform_sphere(num_samples, dim, device=device)
    c = rand_tensor(
        size=(num_samples, 1), low=radius_min**dim, high=radius**dim, device=device
    )
    b = c ** (1 / dim) * s
    return b


def rand_tensor(size, low, high, device="cpu", seed=None):
    """
    Returns random elements between low and high. If low > high, it returns high.

    Parameters
    ----------
    device: string
        device on which generated tensor should be stored
    size: tuple of ints
        size of tensor to generate
    low: {float, Tensor}
        lower bound for random values, shape compatible with size
    high: {float, Tensor}
        upper bound for random values, shape compatible with size
    seed: int
        seed for random number generation, optional

    Returns
    -------
    tensor: Tensor of shape size
        tensor filled with uniform random numbers between low and high
    """
    if np.isscalar(low) and np.isscalar(high):
        factor = max(high - low, 0)
        offset = min(low, high)
    else:
        if np.isscalar(low):
            low = torch.ones(high.shape, device=device) * low
        if np.isscalar(high):
            high = torch.ones(low.shape, device=device) * high
        factor = torch.max(high - low, torch.zeros(low.shape, device=device))
        offset = torch.min(low, high)
    if seed is not None:
        generator = torch.Generator(device=device).manual_seed(seed)
        return torch.rand(size, device=device, generator=generator) * factor + offset
    else:
        return torch.rand(size, device=device) * factor + offset


def uniform_sphere(num_samples: int, dim: int, device="cpu") -> torch.Tensor:
    """Sample points uniformly on a sphere.

    Parameters
    ----------
    num_samples: int
        number of samples to generate
    dim: int
        dimension of ambient space
    device: string
        device on which the returned tensor should be located

    Returns
    -------
    x: tensor of shape [num_samples, dim]
        uniformly distributed points on sphere

    """
    x = torch.normal(0.0, 1.0, size=(num_samples, dim), device=device)
    r = torch.sum(x**2, dim=-1, keepdim=True) ** 0.5
    return x / r


# an alternative might be https://stackoverflow.com/a/16128461/5560137
def regular_sphere(
    num_samples_elev,
    num_samples_azimuth=None,
    upper_hemisphere=True,
    flatten=True,
    elev_range=None,
    azim_range=(0, 2 * np.pi),
    end_points=False,
):
    """Create regular points on the sphere by linearly sampling elevation and azimuth.

    Parameters
    ----------
    num_samples_elev: int
        number of samples for elevation angles
    num_samples_azimuth: int
        number of samples for azimuth angles
    upper_hemisphere: bool
        whether to sample only the upper hemisphere, ignored if elev_range is set
        if true, also include end point (regardless of end_points setting)
    flatten: bool
        whether to flatten returned tensors
    elev_range: tuple of floats
        range of elevation angles
    azim_range: tuple of floats
        range of azimuth angles
    end_points: bool
        whether to include end_points
    Returns
    -------
    xyz_coordinates: torch.Tensor of shape (num_samples_elev*num_samples_azimuth, 3)
        or (num_samples_elev, num_samples_azim, 3) if flatten=False
        xyz spherical coordinates
    angles: torch.Tensor of shape (num_samples_elev*num_samples_azimuth, 2)
        or (num_samples_elev, num_samples_azim, 2) if flatten=False
        elevation, azimuth angles
    """
    if num_samples_azimuth is None:
        num_samples_azimuth = num_samples_elev
    if elev_range is None:
        if upper_hemisphere:
            elev_range = (0, np.pi / 2)
            end_point = True
        else:
            elev_range = (0, np.pi)
            end_point = end_points
    else:
        end_point = end_points
    angle_elev = np.linspace(
        elev_range[0], elev_range[1], num_samples_elev, endpoint=end_point
    ).reshape(-1, 1)
    azim_angle = np.linspace(
        azim_range[0], azim_range[1], num_samples_azimuth, endpoint=end_points
    ).reshape(1, -1)
    sin_theta = (1 - np.cos(angle_elev) ** 2) ** 0.5  # (num_samples_elev, 1)
    x = sin_theta * np.cos(azim_angle)  # (num_samples_elev, num_samples_azimuth)
    y = sin_theta * np.sin(azim_angle)
    z = np.broadcast_to(np.cos(angle_elev), y.shape)
    xyz = np.stack([x, y, z], axis=2)  # (num_samples_elev, num_samples_azimuth, 3)
    angles = np.stack(
        [np.broadcast_to(angle_elev, z.shape), np.broadcast_to(azim_angle, z.shape)],
        axis=2,
    )
    if flatten:
        return xyz.reshape(-1, 3), angles.reshape(-1, 2)
    else:
        return xyz, angles
