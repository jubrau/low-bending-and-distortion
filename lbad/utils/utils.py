from collections import defaultdict

import numpy as np
import torch
from sklearn.utils.extmath import stable_cumsum


def to_torch_and_device(data, device="cpu"):
    """Convert data array to PyTorch float32 tensor and move to device.

    Parameters
    ----------
    data: numpy array or tensor
        array to process
    device: string
        name of device

    Returns
    -------
    data: torch.tensor
        processed tensor

    """
    if not torch.is_tensor(data):
        data = torch.from_numpy(data).to(device=device, dtype=torch.float32)
    elif device is not None:
        data = data.to(device).to(torch.float32)
    return data


def image_to_torch_and_device(data, add_channels=True, device="cpu"):
    """Convert data to PyTorch float32 tensor and move to device.

    Optionally add channels in dim 1 or move channel dimension.

    Parameters
    ----------
    data: numpy array or tensor of shape {(n_samples, C, H, W), (n_samples, H, W),
    (n_samples, H, W, C)}, where C <=4
        array to process
    add_channels: bool
        if true, add channel dimension
    device: string
        name of device

    Returns
    -------
    data: torch.tensor
        tensor of shape (n_samples, C, H ,W)

    """
    if not torch.is_tensor(data):
        data = torch.from_numpy(data).to(device=device, dtype=torch.float32)
    else:
        data = data.to(device)
    if add_channels and data.ndim == 3:
        data = torch.unsqueeze(data, dim=1)
    elif data.shape[-1] <= 4:
        data = torch.movedim(data, -1, 1)[:, :3]
    return data


def add_batch_dim(tensor):
    """Add batch dimension in front.

    Parameters
    ----------
    tensor: tensor of shape {(n_samples, X, Y, Z, ...), (X, Y, Z)}
        input tensor

    Returns
    -------
    tensor: tensor of shape (1, X, Y, Z) if previous shape was (X, Y, Z),
        else keeps shape
        output tensor

    """
    if tensor.ndim == 3:
        tensor = tensor.unsqueeze(0)
    return tensor


def batched_forward_tuples(
    dataloader, network, used_images=("x", "y", "interpolations")
) -> dict:
    """
    Calculate network outputs for all elements in dataloader.

    Parameters
    ----------
    dataloader: Dataloader
        dataloader for dataset returning triples
    network: Module
        any neural network
    used_images: tuple of strings
        names of the elements in the triple to take into account

    Returns
    -------
    results_dict: dict of numpy arrays
        keys are elements in used_images
    """
    results_dict = defaultdict(lambda: [])
    try:
        network.eval()
    except AttributeError:
        pass
    with torch.no_grad():
        for sample in dataloader:
            for key in used_images:
                results_dict[key].append(network(sample[key]).detach().cpu().numpy())
            torch.cuda.empty_cache()
    for key, value in results_dict.items():
        value_mod = np.concatenate(value, axis=0)
        value_mod = np.reshape(value_mod, (value_mod.shape[0], -1))
        results_dict[key] = value_mod
    return results_dict


def batched_forward(dataloader, cuda_function, flatten=True) -> np.ndarray:
    """
    Calculate network outputs for all elements in dataloader.

    Parameters
    ----------
    dataloader: Dataloader
        dataloader for dataset returning tensors
    cuda_function: function processing cuda tensors
        any function processing cuda tensors, e.g. a neural network
    flatten: bool
        whether to flatten the output

    Returns
    -------
    results: numpy array of shape (dataloader.num, network output size)
    where network output size = prod(network output shape)
    """
    results = []
    try:
        cuda_function.eval()
    except AttributeError:
        pass
    with torch.no_grad():
        for sample in dataloader:
            results.append(cuda_function(sample).detach().cpu().numpy())
            torch.cuda.empty_cache()
    value_mod = np.concatenate(results, axis=0)
    if flatten:
        value_mod = np.reshape(value_mod, (value_mod.shape[0], -1))
    return value_mod


def matrix_inv_2d_batched(A):
    det = A[:, 0, 0] * A[:, 1, 1] - A[:, 1, 0] * A[:, 0, 1]
    det = det.unsqueeze(-1).unsqueeze(-1)
    B = torch.empty(A.shape, device=A.device)
    B[:, 0, 0] = A[:, 1, 1]
    B[:, 1, 1] = A[:, 0, 0]
    B[:, 0, 1] = -A[:, 0, 1]
    B[:, 1, 0] = -A[:, 1, 0]
    return B / det


def initialize(layers, slope=0.2):
    """
    Initialize all weights of layers using Kaiming normal initialization for weights and
    zero initialization for biases.
    Used for layers which are followed by LeakyReLU activation functions or similar.

    Parameters
    ----------
    layers: list of pytorch layers
        list of layers to be initialized
    slope: float
        slope to use in Kaiming initialization (i.e. slope of the LeakyReLU following
        the layers)
    """
    for layer in layers:
        if layer.__class__.__name__ not in ["PReLU", "BatchNorm2d"]:
            if hasattr(layer, "weight"):
                w = layer.weight.data
                torch.nn.init.kaiming_normal_(w, a=slope)
            if hasattr(layer, "bias"):
                layer.bias.data.zero_()


def get_num_components(exp_var_ratio: np.ndarray, thres: float) -> int:
    """
    Get number of components needed to explain thres amount of variance.

    Parameters
    ----------
    exp_var_ratio: np.ndarray of shape (n,)
        array of explained variance percentage per components,
        obtained from pca.explained_variance_ratio_
    thres: float
        amount of explained variance, e.g. 0.99

    Returns
    -------
    num_components: int
        number of components needed to explain specified amount of variance

    """
    return np.min(np.argwhere(stable_cumsum(exp_var_ratio) >= thres)) + 1
