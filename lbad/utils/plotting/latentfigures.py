import numpy as np
import seaborn as sns
import matplotlib.colors as clr
import matplotlib.pyplot as plt

color_palette = sns.color_palette("colorblind").as_hex()
colors = {
    "vivid_orange": color_palette[1],
    "desat_magenta": color_palette[4],
    "dark_cyan_lime": color_palette[2],
    "soft blue": color_palette[9],
    "dark_blue": color_palette[0],
    "pantone_black": "#3e3e3b",
}


def make_z_color(values, num_repeats=None, vmin=None, vmax=None):
    """Cycle third entry of values in the range of vmin to vmax num_repeat times.

    If num_repeats is None, values are just scaled between vmin and vmax.

    Parameters
    ----------
    values: np.ndarray of shape [n, 3] or [n, 1]
        coordinates
    num_repeats: int
        number of repeat, optional, default: None
    vmin: float
        minimum value
    vmax: float
        maximum value

    Returns
    -------
    color: np.ndarray of shape [n,]
        normalized values between 0 and 1

    """
    if values.shape[1] > 1:
        c1 = values[:, 2]
    else:
        c1 = values[:, 0]
    if vmin is None:
        vmin = np.min(c1)
    if vmax is None:
        vmax = np.max(c1)
    if np.any(c1 < vmin) or np.any(c1 > vmax):
        raise ValueError("vmin or vmax not appropriate")
    c1 = c1 - vmin
    c1 = c1 / (vmax - vmin)
    if num_repeats is not None:
        return c1 * num_repeats % 1
    else:
        return c1


def greenblue_cmap():
    custom_cmap = clr.LinearSegmentedColormap.from_list(
        "greenblue", colors=[colors["dark_cyan_lime"], colors["soft blue"]][::-1]
    )
    return custom_cmap


def plot_latent_manifold(
    points,
    depthshade=True,
    vmin=None,
    vmax=None,
    opacity=0.2,
    s=20,
    figsize=None,
    fig=None,
    annotate_axes=True,
    edges=False,
):
    """Plot latent manifold.

    Parameters
    ----------
    points: numpy array of shape [num_samples, n]
        points to plot
    depthshade: bool
        whether to shade the scatter markers to give the appearance of depth.
    vmin: float
        minimum value to map
    vmax: float
        maximum value to map
    opacity: float, optional, default: 0.2
        opacity of scatter markers
    s: int
        marker size in points**2
    figsize: tuple of floats, optional
        figure dimension (width, height) in inches
    fig: matplotlib figure with 3d axes
        figure to put scatter markers on
    Returns
    -------
    fig: matplotlib figure

    """
    custom_cmap = greenblue_cmap()
    color = custom_cmap(make_z_color(points, 1, vmin, vmax))
    color[:, 3] = opacity
    if not edges:
        edgecolor = color.copy()
        edgecolor[:, 3] = 0
    if fig is None:
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111, projection="3d")
    else:
        ax = fig.gca()
    # background points
    if not edges:
        ax.scatter(
            *points.T[:3], c=color, depthshade=depthshade, s=s, edgecolors=edgecolor
        )
    else:
        ax.scatter(*points.T[:3], c=color, depthshade=depthshade, s=s)
    if not annotate_axes:
        ax.axes.xaxis.set_ticklabels([])
        ax.axes.yaxis.set_ticklabels([])
        ax.axes.zaxis.set_ticklabels([])
    return fig


def make_latent_space_figure(
    pca_codes,
    points=None,
    start=None,
    middle=None,
    end=None,
    annotate_points=False,
    depthshade=True,
    vmin=None,
    vmax=None,
    opacity=0.2,
    s=20,
    figsize=None,
    annotate_axes=False,
    edges=False,
):
    # todo update documentation
    """
    Make a plot with a collection of latent space codes in first color, some random
    points larger in another color,
    and points start, middle and end larger in a third color.
    :param pca_codes: numpy array of shape (num_samples, n_dims)
    :param points: numpy array of shape (num_samples, n_dims)
    :param start: numpy array of shape (m, n_dims)
    :param middle: numpy array of shape (m, n_dims)
    :param end: numpy array of shape (m, n_dims)
    :param opacity: opacity of points
    :return: matplotlib figure
    """
    # background points
    fig = plot_latent_manifold(
        pca_codes,
        depthshade=depthshade,
        vmin=vmin,
        vmax=vmax,
        opacity=opacity,
        s=s,
        figsize=figsize,
        annotate_axes=annotate_axes,
        edges=edges,
    )
    splines = []
    if start is not None:
        for i in range(len(start)):
            splines.append(
                make_spline(
                    np.stack([start[i][:3], middle[i][:3], end[i][:3]], axis=0).T
                )
            )
    ax = fig.gca()
    # some additional points in different color
    if points is not None:
        ax.scatter(*points.T[:3], alpha=1.0, c=colors["vivid_orange"])
    # triples
    if start is not None:
        for i in range(len(start)):
            ax.scatter(*start[i][:3], alpha=1, c=colors["desat_magenta"])
            ax.scatter(*middle[i][:3], alpha=1, c=colors["desat_magenta"])
            ax.scatter(*end[i][:3], alpha=1, c=colors["desat_magenta"])
            ax.plot(*splines[i], c=colors["desat_magenta"], linewidth=2, alpha=1)
    if annotate_points:
        if start is not None:
            annotate(ax, start[:, :3], prefix="s", colors=colors["desat_magenta"])
        if points is not None:
            annotate(ax, points[:, :3], prefix="p", colors=colors["vivid_orange"])
    return fig


def annotate(ax, points, labels=None, colors=colors["pantone_black"], prefix=""):
    """Annotate points by putting text at position.

    Parameters
    ----------
    ax: maplotlib axes
        ax to put text on
    points: np.ndarray of shape [n,3]
        positions to put text
    labels: list of length n, optional
        labels, if None: prefix0,...,prefix{n-1}
    colors: list of length n, or one color
        colors
    prefix: string, optional, default: ""
        prefix before label
    """
    if labels is None:
        labels = [f"{prefix}{i}" for i in range(len(points))]
    if isinstance(colors, str):
        colors = [colors] * len(points)
    for p, label, color in zip(points, labels, colors):
        ax.text(*p, label, c=color)


def set_limit_and_aspect(
    fig,
    points,
    symmetry_x=True,
    symmetry_y=True,
    x_equals_y=True,
    decimals=None,
    scale=1.1,
    remove_ticklabels=False,
):
    has_z = points.shape[1] >= 3
    x_min, x_max = np.min(points[:, 0] * scale), np.max(points[:, 0] * scale)
    y_min, y_max = np.min(points[:, 1] * scale), np.max(points[:, 1] * scale)
    if has_z:
        z_min, z_max = np.min(points[:, 2] * scale), np.max(points[:, 2] * scale)
    if symmetry_x:
        x_max = decimal_ceil(max(abs(x_min), x_max), decimals)
        x_min = -x_max
    else:
        x_max = decimal_ceil(x_max, decimals)
        x_min = decimal_floor(x_min, decimals)
    if symmetry_y:
        y_max = decimal_ceil(max(abs(y_min), y_max), decimals)
        y_min = -y_max
    else:
        y_max = decimal_ceil(y_max, decimals)
        y_min = decimal_floor(y_min, decimals)
    if x_equals_y:
        x_min = min(x_min, y_min)
        y_min = x_min
        x_max = max(x_max, y_max)
        y_max = x_max
    x_lim = [x_min, x_max]
    y_lim = [y_min, y_max]
    if has_z:
        z_lim = [z_min, z_max]
        z_min = decimal_floor(z_min, decimals)
        z_max = decimal_ceil(z_max, decimals)
    ax = fig.gca()
    if has_z:
        ax.set_box_aspect([x_max - x_min, y_max - y_min, z_max - z_min])
    else:
        ax.set_box_aspect((y_max - y_min) / (x_max - x_min))
    ax.set_xlim(*x_lim)
    ax.set_ylim(*y_lim)
    if has_z:
        ax.set_zlim(*z_lim)
    if remove_ticklabels:
        ax.axes.xaxis.set_ticklabels([])
        ax.axes.yaxis.set_ticklabels([])
        if has_z:
            ax.axes.zaxis.set_ticklabels([])
    if has_z:
        return x_lim, y_lim, z_lim
    else:
        return x_lim, y_lim


def decimal_ceil(x, decimals=1):
    if decimals is None:
        return x
    else:
        return np.ceil(x * 10**decimals) / 10**decimals


def decimal_floor(x, decimals=1):
    if decimals is None:
        return x
    else:
        return np.floor(x * 10**decimals) / 10**decimals


def make_spline(points, k=2, num_points=50):
    # https://stackoverflow.com/questions/18962175/spline-interpolation-coefficients-of-a-line-curve-in-3d-space
    from scipy import interpolate

    tck, u = interpolate.splprep(points, s=0, k=k)
    u_fine = np.linspace(0, 1, num_points)
    x_fine, y_fine, z_fine = interpolate.splev(u_fine, tck)
    return np.stack([x_fine, y_fine, z_fine], axis=0)
