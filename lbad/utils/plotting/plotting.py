import math
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
from matplotlib.figure import figaspect
import io


def make_mosaic(x, nx=None, ny=None, border=0, border_value=1):
    """
    Make a mosaic image out of list of images x. If nx and ny are not specified,
    they are determined automatically by find_rectangle(n).
    :param x: list of images x, list or array of shape (n, h, w) or (n, h, w, c)
    :param nx: number of columns
    :param ny: number of rows
    :param border: size in pixels of border
    :param border_value: value with which to pad
    :return: array of shape nx*w, ny*h
    """
    if not isinstance(x, np.ndarray):
        x = np.asarray(x)

    has_channels = len(x.shape) > 3

    if border > 0:
        if has_channels:
            x = np.pad(
                x,
                [(0, 0), (0, border), (0, border), (0, 0)],
                mode="constant",
                constant_values=border_value,
            )
        else:
            x = np.pad(
                x,
                [(0, 0), (0, border), (0, border)],
                mode="constant",
                constant_values=border_value,
            )

    n, h, w = x.shape[:3]
    if has_channels:
        c = x.shape[3]

    if nx is None and ny is None:
        ny, nx = find_rectangle(n)
    elif ny is None:
        ny = n // nx
    elif nx is None:
        nx = n // ny

    end_shape = (w, c) if has_channels else (w,)
    mosaic = x.reshape(ny, nx, h, *end_shape)
    mosaic = mosaic.swapaxes(1, 2)  # (ny, h, nx, w, c)
    hh = mosaic.shape[0] * mosaic.shape[1]  # ny*h
    ww = mosaic.shape[2] * mosaic.shape[3]  # nx*w
    end_shape = (ww, c) if has_channels else (ww,)
    mosaic = mosaic.reshape(hh, *end_shape)
    return mosaic


def find_rectangle(n):
    """
    Find integers w, h as close to sqrt(n) as possible such that w*h=n.
    :param n: n > 0
    :return: (h, w)
    """
    max_side = int(math.sqrt(n))
    for h in range(2, max_side + 1)[::-1]:
        w = n // h
        if (h * w) == n:
            return (h, w)
    return (1, n)


def plot_triples(
    samples_dict: dict, indices: list[int] = None, cmap="Greys_r", rgb=False
):
    """Plot triples of images.

    Parameters
    ----------
    samples_dict: dict with keys 'x', 'interpolations', 'y', 'distances'
        dictionary of image triples
    indices: list
        list of indices to plot
    cmap: string
        colormap to use if the images don't have color channels
    rgb: bool
        if true, images have a color channel

    Returns
    -------
    fig: matplotlib figure
        figure of triples, where each row is labeled with the distance between pairs

    """
    if indices is None:
        indices = np.arange(len(samples_dict["x"]))
    w, h = figaspect(len(indices) / 3)
    fig, axs = plt.subplots(len(indices), 3, figsize=(w, h))
    if len(indices) == 1:
        axs = np.expand_dims(axs, 0)
    axoff_fun(axs, keep_border=True)
    for i, index in enumerate(indices):
        for j, name in enumerate(["x", "interpolations", "y"]):
            if not rgb:
                axs[i, j].imshow(
                    samples_dict[name][index].cpu().numpy().squeeze(),
                    cmap=cmap,
                    origin="lower",
                )
            else:
                axs[i, j].imshow(channels_last(samples_dict[name][index].cpu().numpy()))
        # axs[i, 0].set_ylabel(f'{samples_dict["distances"][index]: .2f}')
    add_headers(
        fig,
        row_headers=[
            f"{dist ** 0.5:.2f}" for dist in samples_dict["distances"][indices]
        ],
    )
    return fig


def add_headers(
    fig,
    *,
    row_headers: list[str] = None,
    col_headers: list[str] = None,
    row_pad: int = 1,
    col_pad: int = 5,
    rotate_row_headers=True,
    **text_kwargs,
):
    """

    Parameters
    ----------
    fig: matplotlib figure
        the figure which contains the axes to work on
    row_headers: list of strings
        a sequence of strings to be row headers
    col_headers: list of strings
        a sequence of strings to be column headers
    row_pad: int
        value to adjust padding
    col_pad: int
        value to adjust padding
    rotate_row_headers: bool
        whether to rotate by 90° the row headers
    text_kwargs: kwargs
        forwarded to ax.annotate(...)

    Returns
    -------

    """
    # Based on https://stackoverflow.com/a/25814386

    axes = fig.get_axes()

    for ax in axes:
        sbs = ax.get_subplotspec()

        # Putting headers on cols
        if (col_headers is not None) and sbs.is_first_row():
            ax.annotate(
                col_headers[sbs.colspan.start],
                xy=(0.5, 1),
                xytext=(0, col_pad),
                xycoords="axes fraction",
                textcoords="offset points",
                ha="center",
                va="baseline",
                **text_kwargs,
            )

        # Putting headers on rows
        if (row_headers is not None) and sbs.is_first_col():
            ax.annotate(
                row_headers[sbs.rowspan.start],
                xy=(0, 0.5),
                xytext=(-ax.yaxis.labelpad - row_pad, 0),
                xycoords=ax.yaxis.label,
                textcoords="offset points",
                ha="right",
                va="center",
                rotation=rotate_row_headers * 90,
                **text_kwargs,
            )


def channels_last(image):
    """Permutes dimensions of image such that channels (dimension with size 3) are last.
    Also squeezes image.

    Parameters
    ----------
    image: {torch.tensor, np.ndarray} of shape [c, h, w], [n, c, h, w] or with
        additional dimension of size 1
        image to process

    Returns
    -------
    image: {torch.tensor, np.ndarray} of shape [h, w, c] or [n, h, w, c] if n > 1
    """
    image = image.squeeze()
    if len(image.shape) == 3:
        if image.shape[0] == 3:
            if isinstance(image, np.ndarray):
                return image.transpose([1, 2, 0])
            else:
                return image.permute([1, 2, 0])
        else:
            return image

    elif len(image.shape) == 4:
        if image.shape[1] == 3:
            if isinstance(image, np.ndarray):
                return image.transpose([0, 2, 3, 1])
            else:
                return image.permute([0, 2, 3, 1])
    else:
        return image


def axoff_fun(ax, keep_border=False, keep_facecolor=False):
    if keep_border or keep_facecolor:

        def turn_off(p_ax):
            p_ax.set_xticks([])
            p_ax.set_yticks([])
            try:
                p_ax.set_zticks([])
            except AttributeError:
                pass
            if not keep_border:
                try:
                    p_ax.xaxis.line.set_color((1.0, 1.0, 1.0, 0.0))
                    p_ax.yaxis.line.set_color((1.0, 1.0, 1.0, 0.0))
                    p_ax.zaxis.line.set_color((1.0, 1.0, 1.0, 0.0))
                except AttributeError:
                    p_ax.spines["top"].set_visible(False)
                    p_ax.spines["right"].set_visible(False)
                    p_ax.spines["bottom"].set_visible(False)
                    p_ax.spines["left"].set_visible(False)
            if not keep_facecolor:
                try:
                    p_ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
                    p_ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
                    p_ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
                except AttributeError:
                    p_ax.set_facecolor((1.0, 1.0, 1.0, 0.0))

        try:
            for ax_ in ax.flatten():
                turn_off(ax_)
        except AttributeError:
            turn_off(ax)
    else:
        return np.vectorize(lambda ax: ax.axis("off"))(ax)


def fig2img(fig, dpi=400, crop=True):
    """Convert a Matplotlib figure to a PIL Image, crop away transparent regions and
    return it.

    Parameters
    ----------
    fig: matplotlib figure
        figure to convert
    dpi: int
        dpi
    crop: boolean
        if True, crop away transparent regions

    Returns
    -------
    img: PIL image
        figure as pillow image
    """
    buf = io.BytesIO()
    fig.savefig(buf, pad_inches=0, bbox_inches="tight", dpi=dpi, transparent=True)
    buf.seek(0)
    img = Image.open(buf)
    if crop:
        img = img.crop(img.getbbox())
    return img


def crop_white(images, thres=0.3, outliers_thres=None):
    def ignore_outliers(data, m):
        if m is not None:
            return np.where(
                abs(data - np.mean(data, axis=0)) < m * np.std(data, axis=0),
                data,
                np.mean(data, axis=0, keepdims=True),
            )
        else:
            return data

    images = channels_last(images)
    masked = np.sum(np.abs(1 - images), axis=-1)
    relevant_coords = [
        ignore_outliers(np.argwhere(mask > thres), m=outliers_thres).astype("int")
        for mask in masked
    ]
    images_cropped = []
    for coords, img in zip(relevant_coords, images):
        images_cropped.append(
            img[
                min(coords[:, 0]) : max(coords[:, 0]),
                min(coords[:, 1]) : max(coords[:, 1]),
                :,
            ]
        )
    return images_cropped
