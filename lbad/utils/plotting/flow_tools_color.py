import numpy as np


def plot_color_wheel(h, w, max_flow=1):
    x, dx = np.linspace(-max_flow, max_flow, w, retstep=True)
    y, dy = np.linspace(-max_flow, max_flow, h, retstep=True)
    xv, yv = np.meshgrid(x, y, indexing="xy")
    xy = np.stack([xv, yv], axis=2)
    dist = np.sqrt(xv**2 + yv**2)
    xy[dist >= max_flow, :] = 0
    # use origin=lower and the following for plotting
    #  ax.spines['top'].set_visible(False)
    #  ax.spines['right'].set_visible(False)
    return computeFlowImg(xy)


def makeColorWheel():
    RY = 15
    YG = 6
    GC = 4
    CB = 11
    BM = 13
    MR = 6

    size = RY + YG + GC + CB + BM + MR
    colorwheel = np.zeros((3, size))

    col = 0
    # RY
    colorwheel[0, col : col + RY] = 255
    colorwheel[1, col : col + RY] = np.floor(255 * np.arange(RY) / RY)
    col += RY

    # YG
    colorwheel[0, col : col + YG] = 255 - np.floor(255 * np.arange(YG) / YG)
    colorwheel[1, col : col + YG] = 255
    col += YG

    # GC
    colorwheel[1, col : col + GC] = 255
    colorwheel[2, col : col + GC] = np.floor(255 * np.arange(GC) / GC)
    col += GC

    # CB
    colorwheel[1, col : col + CB] = 255 - np.floor(255 * np.arange(CB) / CB)
    colorwheel[2, col : col + CB] = 255
    col += CB

    # BM
    colorwheel[0, col : col + BM] = np.floor(255 * np.arange(BM) / BM)
    colorwheel[2, col : col + BM] = 255
    col += BM

    # MR
    colorwheel[0, col : col + MR] = 255
    colorwheel[2, col : col + MR] = 255 - np.floor(255 * np.arange(MR) / MR)

    return colorwheel.astype("uint8")


def computeNormalizedFlow(u, v, max_flow=-1, min_max_flow=-1):
    """
    This function normalizes the optical flow field.
    If the flow value lies above the UNKNOWN_FLOW_THRES it will be considered invalid
    (e.g. occluded)

    Normalization means that the `max_flow` value is linearly scaled to be 1.
    `max_flow` can either be specified externally, or calculated from the maximum
    length of the flow vector.
    If a min_max_flow is specified (>0) then the maximum flow, then the estimated
    maximum flow will be no smaller than this value.
    This limits amplified visualization of small noise when no flow is present
    """

    eps = 1e-15
    UNKNOWN_FLOW_THRES = 1e9

    # fix unknown flow
    idxUnknown = np.where(
        np.logical_or(np.abs(u) > UNKNOWN_FLOW_THRES, np.abs(v) > UNKNOWN_FLOW_THRES)
    )
    u[idxUnknown] = 0
    v[idxUnknown] = 0

    if max_flow < 0:
        rad = np.sqrt(u**2 + v**2)
        if min_max_flow >= 0:
            rad = np.max(
                (np.max(rad), min_max_flow)
            )  # lower bound for max_flow => don't amplify noise
    else:
        rad = max_flow  # biggest allowed flow = max_flow
    max_rad = np.max(rad)

    u = u / (max_rad + eps)
    v = v / (max_rad + eps)

    return u, v


def computeFlowImg(flow, max_flow=-1, min_max_flow=-1):
    """
    Overview:
        -------
        This function calculates a colorized flow image for a given flow field.

        For the colorization flow values are being normalized to 1 before converted to
        the color wheel.
        If `max_flow` =-1 the maximum flow vector is used for the normalization.
        If a specific value is passed for `max_flow` the flow field is normalized by
        this value.

        If automatic normalization (`max_flow`=-1) is used, a lower threshold for the
        normalization value can be specified. This can be used to avoid amplified
        visualization of noise if no flow is present.

    Usage:
    -------
        >> # load a 2D flow field  shape [y_dim, x_dim, 2]
        >> flow2D = load_flow_data("test.flo")

        >> #compute a color image for the optical flow
        >> img = computeFlowImg( flow2D)

        >> #Show the image
        >> plt.imshow(img)

    Type hints:
        -------
        @type flow : numpy.ndarray
        @type max_flow : int
        @type min_max_flow : int
        @rtype numpy.ndarray

    """

    u, v = flow[:, :, 0], flow[:, :, 1]

    u, v = computeNormalizedFlow(u, v, max_flow, min_max_flow)

    nanIdx = np.logical_or(np.isnan(u), np.isnan(v))
    u[np.where(nanIdx)] = 0
    v[np.where(nanIdx)] = 0

    cw = makeColorWheel().T

    M, N = u.shape
    img = np.zeros((M, N, 3)).astype("uint8")

    mag = np.sqrt(u**2 + v**2)

    phi = np.arctan2(-v, -u) / np.pi  # [-1, 1]
    phi_idx = (phi + 1.0) / 2.0 * (cw.shape[0] - 1)
    f_phi_idx = np.floor(phi_idx).astype("int")

    c_phi_idx = f_phi_idx + 1
    c_phi_idx[c_phi_idx == cw.shape[0]] = 0

    floor = phi_idx - f_phi_idx

    for i in range(cw.shape[1]):
        tmp = cw[:, i]

        # linear blend between colors
        col0 = (
            tmp[f_phi_idx] / 255.0
        )  # from colorwheel take specified values in phi_idx
        col1 = tmp[c_phi_idx] / 255.0
        col = (1.0 - floor) * col0 + floor * col1

        # increase saturation for small magnitude
        sat_idx = np.where(mag <= 1)
        non_sat_idx = np.where(mag > 1)
        col[sat_idx] = 1 - mag[sat_idx] * (1 - col[sat_idx])

        col[non_sat_idx] = col[non_sat_idx] * 0.75

        img[:, :, i] = (np.floor(255.0 * col * (1 - nanIdx))).astype("uint8")
    return img


def max_flow_norm(flow: np.ndarray) -> float:
    """
    Calculate the maximum norm of flow vectors
    :param flow: numpy array of shape (..., 2)
    :return: maximum norm
    """
    rad = flow[..., 0] ** 2 + flow[..., 1] ** 2
    max_displacement = np.max(np.sqrt(rad))
    return max_displacement
