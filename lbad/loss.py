import torch


def bending_distortion_loss(lambda_hess, lambda_grad=1, sum_up=True, c=0):
    """
    Bending distortion loss function.

    Parameters
    ----------
    lambda_hess: float
        penalty in front of hessian term
    lambda_grad: float, optional, default:1
        penalty in front of gradient term
    sum_up: boolean
        if true, function returns scalars, otherwise pointwise errors
    c: float
        parameter in gamma (isometry loss penalty function)

    Returns
    -------
    fun: loss function

    """

    def gamma(s_squared):
        return s_squared + (1 + c**2) ** 2 / (s_squared + c**2) - 2 - c**2

    def fun(
        interpolated_code, code_of_interpolation, dist_manifold_squared, x_code, y_code
    ):
        """

        Parameters
        ----------
        interpolated_code: torch.tensor
            euclidean average between encoder(x), encoder(y)
        code_of_interpolation: torch.tensor of same shape as interpolated code
            encoder(riemannian average of x, y)
        dist_manifold_squared:  torch.tensor of shape {(batch_dim,), (batch_dim, 1)}
            squared distance of x, y on the manifold
        x_code: torch.tensor of same shape as interpolated code
            encoder(x)
        y_code: torch.tensor of same shape as interpolated code
            encoder(y)

        Returns
        -------
        losses: dict
            hess: |second order difference quotient|^2
            grad_gamma: gamma(|first order difference quotient|)
            grad: |first order difference quotient|^2 - 1
            grad_inv: 1/|first order different quotient|^2 - 1
            latent_total: lambda_hess * hess + lambda_grad * grad_gamma
            hess_w: hess*lambda_hess
            grad_gamma_w: grad_gamma*lambda_grad

        """
        # dist manifold is squared distance
        dist_manifold_squared = (
            dist_manifold_squared.squeeze()
        )  # if we have an additional 1, broadcasting breaks
        interp_loss = L2(
            interpolated_code - code_of_interpolation, batch_sum=False, mean=False
        )
        dist_latent_squared = L2(x_code - y_code, batch_sum=False, mean=False)
        loss_hessian = interp_loss / dist_manifold_squared**2
        loss_grad = dist_latent_squared / dist_manifold_squared
        loss_grad_gamma = gamma(loss_grad)
        loss_total = lambda_hess * loss_hessian + lambda_grad * loss_grad_gamma
        if sum_up:
            losses = {
                "latent_total": torch.sum(loss_total),
                "hess": torch.sum(loss_hessian),
                "grad": torch.sum(loss_grad - 1),
                "grad_inv": torch.sum(1 / loss_grad - 1),
                "grad_gamma": torch.sum(loss_grad_gamma),
            }
        else:
            losses = {
                "latent_total": loss_total,
                "hess": loss_hessian,
                "grad": loss_grad - 1,
                "grad_inv": 1 / loss_grad - 1,
                "grad_gamma": loss_grad_gamma,
            }
        losses["hess_w"] = lambda_hess * losses["hess"]
        losses["grad_gamma_w"] = lambda_grad * losses["grad_gamma"]
        return losses

    return fun


def L2(x, batch_sum=True, mean=True):
    """
    Sum over squared differences, divided by number of entries (but not divided by batch
    size).
    """
    if batch_sum:
        if mean:
            return torch.mean(x**2) * x.shape[0]
        else:
            return torch.sum(x**2)
    else:
        non_batch_dims = list(range(1, len(x.shape)))
        if mean:
            return torch.mean(x**2, dim=non_batch_dims)
        else:
            return torch.sum(x**2, dim=non_batch_dims)
