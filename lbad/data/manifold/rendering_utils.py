import torch

from lbad.utils import to_torch_and_device, matrix_inv_2d_batched


def render_via_implicit(
    impl_func,
    pos: torch.Tensor,
    angle: torch.Tensor,
    shift_x=None,
    shift_y=None,
    plotting_format=True,
    k: float = 0.0,
    scale: float = 1.0,
    **func_args
):
    """Render the region described by an implicit function,
    rotated by angle and shifted.

    Parameters
    ----------
    pos: torch.Tensor of shape [h, w, 2, 1]
        grid of positions on which to evaluate
    angle: torch.tensor of shape [n, ]
        rotation angle
    shift_x: torch.Tensor of shape [n, 1]
        x position of astroid
    shift_y: torch.Tensor of shape [n, 1]
        y position of astroid
    plotting_format: boolean
        if true, don't add channel to output
    k: float, default: 0
        smoothness parameters

    Returns
    -------
    val: torch.Tensor of shape [n, h, w] or [n, 1, h, w] if plotting_format=False

    """
    if shift_x is None:
        shift_x = torch.zeros((len(angle), 1), device=pos.device)
    if shift_y is None:
        shift_y = torch.zeros((len(angle), 1), device=pos.device)
    shift = torch.cat([shift_x, shift_y], dim=-1)  # shape (n, 2)
    shift = shift[:, None, None, :, None]  # shape (n, 1, 1, 2, 1)
    rot = rotation_matrix(-angle)  # shape (n, 2, 2)
    pos = pos[None, ..., None]  # shape (n, h, w, 2, 1)
    rot = rot[:, None, None, ...]  # shape (n, 1, 1, 2, 2)
    pos = rot @ ((pos - shift) / scale)
    pos_x = pos[..., 0, 0]
    pos_y = pos[..., 1, 0]
    val = impl_func(pos_x, pos_y, **func_args)
    if k is not None:
        val = heaviside_approx(val, k=k)
    if not plotting_format:
        val = torch.unsqueeze(val, dim=1)
    return val


def astroid(pos_x, pos_y, radius=1):
    return (
        pos_x**2 + pos_y**2 - radius**2
    ) ** 3 + 27 * radius**2 * pos_x**2 * pos_y**2


def ellipse(pos_x, pos_y, aspect):
    return pos_x**2 + (aspect * pos_y) ** 2 / 1**2 - 1


def render_ellipse_cov(
    pos: torch.Tensor,
    cov,
    pos_x=None,
    pos_y=None,
    plotting_format=True,
    invert_cov=True,
    k=0,
) -> torch.Tensor:
    """Evaluate ellipse function with given parameters on given positions.

    Parameters
    ----------
    pos: torch.Tensor of shape [h, w, 2]
        grid of positions on which to evaluate
    cov: array of shape [n, 2, 2]
        covariance matrices
    pos_x: array of shape [n, 1]
        x position of ellipse
    pos_y: array of shape [n, 1]
        y position of ellipse
    plotting_format: boolean
        if true, don't add channel to output
    invert_cov: boolean
        if true, use inverted covariance matrix
    k: float
        smoothness parameters

    Returns
    -------
    ellipses: torch.Tensor of shape [n, h, w] or [n, 1, h, w] if plotting_format=False

    """
    if pos_x is None:
        pos_x = torch.zeros((len(cov), 1))
    if pos_y is None:
        pos_y = torch.zeros((len(cov), 1))
    cov = to_torch_and_device(cov, device=pos.device)
    pos_x = to_torch_and_device(pos_x, device=pos.device)
    pos_y = to_torch_and_device(pos_y, device=pos.device)
    ellipses = []
    pos = torch.unsqueeze(pos, -1)
    if invert_cov:
        mat = matrix_inv_2d_batched(cov)
    else:
        mat = cov
    for i in range(len(cov)):
        # pos has shape (m, m, 2, 1)
        p = pos - torch.cat([pos_x[i], pos_y[i]], axis=-1).reshape(1, 1, 2, 1)
        ellipses.append(
            torch.matmul(torch.matmul(p.swapaxes(2, 3), mat[i].reshape(1, 1, 2, 2)), p)
        )
    ellipses = torch.stack(ellipses, axis=0).squeeze(dim=-1).squeeze(dim=-1) - 1
    if k is not None:
        ellipses = heaviside_approx(ellipses, k=k)
    if not plotting_format:
        ellipses = torch.unsqueeze(ellipses, dim=1)
    return ellipses


def heaviside_approx(val, k=0):
    if k > 0:
        val = 1 / (1 + torch.exp(k * val))
    else:
        val = (val <= 0).type(torch.float32)
    return val


def rotation_matrix(angles: torch.Tensor) -> torch.Tensor:
    c, s = torch.cos(angles), torch.sin(angles)
    row1 = torch.stack([c, -s], dim=-1)  # shape (n, 2)
    row2 = torch.stack([s, c], dim=-1)  # shape(n, 2)
    return torch.stack([row1, row2], dim=1)
