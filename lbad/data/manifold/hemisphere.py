import numpy as np
import torch

from lbad.utils.sampling import uniform_sphere, uniform_ball
from lbad.data.manifold.manifold import MidpointManifoldWithNormalCoordinates
from lbad.utils import to_torch_and_device


class Hemisphere(MidpointManifoldWithNormalCoordinates):
    """
    Upper hemisphere, two-dimensional manifold embedded in three-dimensional space.
    """

    def __init__(self):
        self.dim = 2
        self.diameter = np.pi
        self.reach = 1
        self.r0 = np.pi / 2
        self.embed_dim = 3

    def exp(
        self, tangent_vec: torch.Tensor, base_point: torch.Tensor, **kwargs
    ) -> torch.Tensor:
        """Compute exp map of a base point in tangent vector direction.

        Parameters
        ----------
        tangent_vec: tensor of shape {[num_samples, 3], [1, 3]}
            tangent vector at base point
        base_point: tensor of shape {[num_samples, 3], [1, 3]}
            base point

        Returns
        -------
        exp: tensor of shape [num_samples, 3]
            riemannian exponential as on sphere, nan if endpoint is not on the upper
            hemisphere
        """
        norm_v = torch.sum(tangent_vec**2, dim=1, keepdim=True) ** 0.5
        point = base_point * torch.cos(norm_v) + tangent_vec * torch.sinc(
            norm_v / np.pi
        )
        return torch.where(
            point[:, [2]] > -1e-8, point, torch.ones_like(point) * np.nan
        )

    def log(
        self, point: torch.Tensor, base_point: torch.Tensor, **kwargs
    ) -> torch.Tensor:
        eps = np.finfo(np.float64).eps
        xTy = torch.clamp(torch.sum(base_point * point, dim=1, keepdim=True), -1, 1)
        factor = (torch.arccos(xTy) + eps) / ((1 - xTy**2) ** 0.5 + eps)
        return (point - base_point * xTy) * factor

    def tangent_isometry(self, euclidean_vec, base_point, **kwargs):
        """Isometric map between euclidean space and tangent space

        Computed by computing a basis of tangent space.

        Parameters
        ----------
        euclidean_vec: array-like of shape {[num_samples, 2], [1, 2]}
            vector in euclidean space, either one for each base point or broadcast
        base_point: array-like of shape {[num_samples, 3], [1, 3]}
            base point, either one for each vector or broadcast

        Returns
        -------
        tangent_vec: array-like of shape [num_samples, 3]
            tangent vector at base point

        """
        basis = self.tangent_space(base_point)
        tangent_vec = torch.einsum("bi, bik -> bk", euclidean_vec, basis)
        return tangent_vec

    def random_point(self, num_samples: int, device="cpu") -> torch.Tensor:
        sphere_points = uniform_sphere(num_samples, 3)
        sphere_points[:, -1] = np.abs(sphere_points[:, -1])
        return to_torch_and_device(sphere_points, device)

    def squared_dist(self, x: torch.Tensor, y: torch.Tensor, **kwargs) -> torch.Tensor:
        return torch.arccos(torch.sum(x * y, dim=1)) ** 2

    def uniform_cone(
        self, num_samples: int, radius=None, radius_min=0.0, device="cpu"
    ) -> torch.Tensor:
        """Sample a cone in 2-dimensional space.

        In this case, the cone is given as a ball intersected with the upper halfspace
        {z>=0}.

        Parameters
        ----------
        num_samples: int
            number of samples
        radius: float
            maximal radius
        radius_min: float
            minimal radius
        device: string
            device

        Returns
        -------
        samples: torch.Tensor of shape [num_samples, self.dim]
            samples in cone
        """
        if radius > self.r0:
            raise ValueError(
                f"Radius r={radius:.2f} larger than allowed r0={self.r0:.2f}."
            )
        samples = uniform_ball(
            num_samples, self.dim, radius=radius, radius_min=radius_min, device=device
        )
        samples[:, -1] = torch.abs(samples[:, -1])
        return samples

    @staticmethod
    def intrinsic_to_extrinsic(polar_angle, azimuth_angle):
        """Convert polar and azimuth angle representation to x,y,z.

        Parameters
        ----------
        polar_angle: torch.tensor of shape (n,)
            polar angles
        azimuth_angle: torch.tensor of shape (n,)
            azimuth angles

        Returns
        -------
        xyz: torch.tensor of shape (n,3)
            extrinsic coordinates

        """
        z = torch.cos(polar_angle)
        if torch.any(
            torch.logical_and(polar_angle > np.pi / 2, polar_angle < 3 / 2 * np.pi)
        ):
            raise ValueError(
                "Invalid value for polar angles. Should NOT be between " "π/2 and 3/2π."
            )
        x = torch.sin(polar_angle) * torch.cos(azimuth_angle)
        y = torch.sin(polar_angle) * torch.sin(azimuth_angle)
        return torch.stack([x, y, z], dim=-1)

    @staticmethod
    def belongs(x):
        """Check whether x belongs to manifold.

        Parameters
        ----------
        x: torch.Tensor of shape [num_samples, embed_dim]
            points in ambient space

        Returns
        -------
        belong: torch.Tensor of shape [num_samples,]
            boolean tensor, true if point is on manifold
        """
        is_sphere = torch.isclose(
            torch.sum(x**2, dim=1) ** 0.5, torch.tensor(1.0, dtype=x.dtype)
        )
        is_upper = x[:, 2] >= 0
        return torch.logical_and(is_sphere, is_upper)

    @staticmethod
    def extrinsic_to_intrinsic(x):
        if not torch.all(Hemisphere.belongs(x)):
            raise ValueError("Not all points lie on the Hemisphere.")
        azimuth_angle = torch.atan2(x[:, 1], x[:, 0])
        polar_angle = torch.acos(x[:, 2])
        return polar_angle, azimuth_angle

    @staticmethod
    def tangent_space(x: torch.Tensor) -> torch.Tensor:
        """Compute ONB of tangent space at x.

        t1: vector in x-y plane
        t2: missing vector such that z >= 0

        Parameters
        ----------
        x: tensor of shape [num_samples, 3]
            points on manifold

        Returns
        -------
        basis: tensor of shape [num_samples, 2, 3]
            basis of tangent space at x
        """
        cos_theta = x[:, 2]
        theta, phi = Hemisphere.extrinsic_to_intrinsic(x)
        sin_theta = torch.sin(theta)
        cos_phi = torch.cos(phi)
        sin_phi = torch.sin(phi)
        t2 = torch.stack([-cos_phi * cos_theta, -sin_phi * cos_theta, sin_theta], dim=1)
        t1 = torch.stack([sin_phi, -cos_phi, torch.zeros_like(cos_phi)], dim=1)
        tangent_basis = torch.stack([t1, t2], dim=1)
        return tangent_basis

    @staticmethod
    def normal_space(x: torch.Tensor) -> torch.Tensor:
        """Compute ONB of normal space at x.

        Parameters
        ----------
        x: tensor of shape [num_samples, 3]
            points on manifold

        Returns
        -------
        basis: tensor of shape [num_samples, 1, 3]
            basis of normal space at x
        """
        return x[:, None, :]

    @staticmethod
    def projection(point: torch.Tensor) -> torch.Tensor:
        """Compute projection of arbitrary point in ambient space onto manifold.

        Parameters
        ----------
        point: tensor of shape [num_samples, 3]
            points to be projected

        Returns
        -------
        projection: tensor of shape [num_samples, 3]
            projected point
        """
        point_pos = point.clone()
        point_pos[point_pos[:, 2] < 0, 2] = 0
        return point_pos / torch.sum(point_pos**2, dim=1, keepdims=True) ** 0.5

    def data_range(self):
        """Return per feature minimum and maximum.

        Returns
        -------
        data_min, data_max: np.array of shape [self.embed_dim]
            per feature minimum and maximum
        """
        data_min = np.asarray([-1, -1, 0])
        data_max = np.asarray([1, 1, 1])
        return data_min, data_max


class HemisphereProjectionRenderer:
    r"""
    Project a point on the upper hemisphere onto two dimensions by
    :math:`p(x_1, x_2, x_3)=-\frac{\delta}{\delta + x_3}(x_1, x_2)`, where
    :math:`\delta` is the "rod gap".
    For small rod gaps, points concentrate around 0 in two dimensions.
    Render this projected point as a Gaussian with variance sigma_sq.
    """

    def __init__(self, im_width, sigma_sq=0.01, device="cpu", rod_gap=0.1):
        """Initialize.

        Parameters
        ----------
        im_width : int
            width of generated image (square)
        device: string
            device on which computations are made
        sigma_sq: float
            variance of gaussian
        rod_gap: float
            distance of rod to origin, default 0.1
        """
        self.im_width = im_width
        self._device = device
        x = np.linspace(-1, 1, im_width)
        xx, yy = np.meshgrid(x, x, indexing="xy")
        self.xx = torch.tensor(xx, dtype=torch.float32).to(device).unsqueeze(0)
        self.yy = torch.tensor(yy, dtype=torch.float32).to(device).unsqueeze(0)
        self.rod_gap = rod_gap
        self.sigma_sq = sigma_sq

    @property
    def device(self):
        return self._device

    @device.setter
    def device(self, value):
        self._device = value
        self.xx = self.xx.to(value)
        self.yy = self.yy.to(value)

    def sundial_projection(self, x):
        """Takes points on the sphere and projects them onto a plane along a line
        through a rod.

        Parameters
        ----------
        x : torch.Tensor of shape [num_samples, 3]
            points on the sphere

        Returns
        -------
        projected: torch.Tensor of shape [num_samples, 2]
            projected points

        """
        return x[..., :2] * (self.rod_gap / (x[..., [2]] + self.rod_gap))

    def render(self, p, plotting_format=False):
        """Generate an image of a Gaussian circle given points on the upper hemisphere.

        Parameters
        ----------
        p : torch.Tensor of shape [num_samples, 3]
            points on upper hemisphere to be rendered
        plotting_format : bool (default: True)
            specify output format

        Returns
        -------
        images: torch.Tensor of shape [num_samples, 1, im_width, im_width]
            if plotting_format=False,
            else torch.Tensor of shape [num_samples, im_width, im_width]
            rendered images
        """
        p = self.sundial_projection(p)
        p = p.to(self.device)
        p = p.reshape(-1, 2, 1)
        p1 = p[:, [0]]  # (n, 1, 1)
        p2 = p[:, [1]]  # (n, 1, 1)
        val = torch.exp(
            -((self.xx - p1) ** 2 + (self.yy - p2) ** 2) / (2 * self.sigma_sq)
        )
        images = val.reshape(-1, 1, self.im_width, self.im_width)
        if plotting_format:
            images = images.squeeze()
        return images


class SundialRenderer:
    def __init__(self, im_width, shadow_width=0.01, device="cpu", rod_gap=0.1):
        """Initialize.

        Parameters
        ----------
        im_width : int
            width of generated image (square)
        shadow_width: float
            width of the shadow
        device: string
            device on which computations are made
        rod_gap: float
            distance of rod to origin, default 0.1
        """
        self.im_width = im_width
        self.shadow_width = shadow_width
        self._device = device
        x = np.linspace(-1, 1, im_width)
        xx, yy = np.meshgrid(x, x, indexing="xy")
        self.xx = torch.tensor(xx, dtype=torch.float32).to(device).unsqueeze(0)
        self.yy = torch.tensor(yy, dtype=torch.float32).to(device).unsqueeze(0)
        self.rod_gap = rod_gap

    @property
    def device(self):
        return self._device

    @device.setter
    def device(self, value):
        self._device = value
        self.xx = self.xx.to(value)
        self.yy = self.yy.to(value)

    def gaussian_sundial(self, p: torch.Tensor):
        """Generate images of a sundial shadow corresponding to points p on the plane.

        Parameters
        ----------
        p : torch.Tensor of shape [num_samples, 2]
            points on the plane

        Returns
        -------
        images: torch.Tensor of shape [num_samples, 1, im_width, im_width]
            rendered sundial images

        """
        p = p.to(self.device)
        p = p.reshape(-1, 2, 1)
        p1 = p[:, [0]]  # (n, 1, 1)
        p2 = p[:, [1]]  # (n, 1, 1)
        r_sq = p1**2 + p2**2  # (n, 1, 1)
        sigma_y_sq = self.shadow_width**2
        sigma_x_sq = torch.max(
            r_sq / 2,
            other=torch.tensor(sigma_y_sq, dtype=torch.float32, device=self.device),
        )  # (n, 1, 1)
        # https://en.wikipedia.org/wiki/Gaussian_function#Two-dimensional_Gaussian_function
        theta = torch.atan2(p2, p1)  # (n, 1, 1)
        a = torch.cos(theta) ** 2 / (2 * sigma_x_sq) + torch.sin(theta) ** 2 / (
            2 * sigma_y_sq
        )  # (n, 1, 1)
        b = torch.sin(2 * theta) / (4 * sigma_x_sq) - torch.sin(2 * theta) / (
            4 * sigma_y_sq
        )
        c = torch.sin(theta) ** 2 / (2 * sigma_x_sq) + torch.cos(theta) ** 2 / (
            2 * sigma_y_sq
        )
        val = torch.exp(
            -(
                a * (self.xx - p1 / 2) ** 2
                + 2 * b * (self.xx - p1 / 2) * (self.yy - p2 / 2)
                + c * (self.yy - p2 / 2) ** 2
            )
        )
        val = val.reshape(-1, 1, self.im_width, self.im_width)
        return val

    def sundial_projection(self, x):
        """Takes points on the sphere and projects them onto a plane along a line
        through a rod.

        Parameters
        ----------
        x : torch.Tensor of shape [num_samples, 3]
            points on the sphere

        Returns
        -------
        projected: torch.Tensor of shape [num_samples, 2]
            projected points

        """
        return x[..., :2] * (self.rod_gap / (x[..., [2]] + self.rod_gap))

    def render(self, p, plotting_format=False):
        """Generate a sundial image given points on the upper hemisphere.

        Parameters
        ----------
        p : torch.Tensor of shape [num_samples, 3]
            points on upper hemisphere to be rendered
        plotting_format : bool (default: True)
            specify output format

        Returns
        -------
        images: torch.Tensor of shape [num_samples, 1, im_width, im_width]
            if plotting_format=False,
            else torch.Tensor of shape [num_samples, im_width, im_width]
            rendered images
        """
        points_projected = self.sundial_projection(p)
        images = self.gaussian_sundial(points_projected)
        if plotting_format:
            images = images.squeeze()
        return images
