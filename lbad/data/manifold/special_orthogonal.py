from pathlib import Path
from typing import Union

from lbad.data.manifold import MidpointManifoldWithNormalCoordinates
from lbad.data.manifold.rendering_utils_3d import Renderer3d
import numpy as np
from scipy.spatial.transform import Slerp, Rotation as R
import torch
from geomstats.geometry.hypersphere import Hypersphere
from lbad.utils import to_torch_and_device
import geomstats.backend as gs

from lbad.utils.sampling import regular_sphere, uniform_ball


class SO3(MidpointManifoldWithNormalCoordinates):
    def __init__(self, metric_scale=0.5):
        super().__init__()
        n = 3
        self.dim = int(n * (n - 1) / 2)
        self.codim = int(n**2 - self.dim)
        self.diameter = np.pi * (2 * metric_scale) ** 0.5
        self.r0 = self.diameter
        self.s3 = Hypersphere(3)
        self.metric_scale = metric_scale
        self.embed_dim = n**2

    def id(self, quat=False):
        if quat:
            return np.asarray([0, 0, 0, 1])
        else:
            return np.eye(3)

    def exp(
        self,
        tangent_vec: torch.Tensor,
        base_point: torch.Tensor,
        return_type="matrix",
        **kwargs
    ):
        """Compute exp map of a base point in tangent vector direction.

        Tangent vector should be represented in S^3.

        Parameters
        ----------
        tangent_vec: torch.Tensor of shape {[num_samples, 4], [1, 4]}
            tangent vector at base point
        base_point: torch.Tensor of shape [{num_samples, 1}, {4, [3,3]}]
            base point
        return_type: string
            one of "matrix" and "quaternion", representation of returned value

        Returns
        -------
        exp: torch.Tensor of shape [num_samples,3, 3] or [num_samples, 4]
            riemannian exponential
        """
        if tangent_vec.shape[1] != 4:
            raise ValueError("Provide tangent vectors of S^3.")
        if isinstance(tangent_vec, torch.Tensor):
            tangent_vec = tangent_vec.cpu()
        tangent_vec = gs.array(tangent_vec)
        base_point = self.to_quaternion(base_point)
        base_point = gs.array(base_point)
        exp_sphere = self.s3.metric.exp(tangent_vec, base_point)
        exp_sphere = gs.to_numpy(exp_sphere)
        if return_type == "matrix":
            exp_sphere = R.from_quat(exp_sphere).as_matrix()
        return exp_sphere

    def geodesic_interpolation(
        self,
        x: torch.Tensor,
        y: torch.Tensor,
        alphas,
        return_type="matrix",
    ) -> torch.Tensor:
        """Calculate weighted geodesic interpolation between x and y with weight alpha.

        Parameters
        ----------
        x: tensor of shape [num_samples, ...]
            Points on manifold.
        y: tensor of shape [num_samples, ...]
            Points on manifold.
        alphas: tensor of shape [num_samples, ...] or float
            Interpolation weight.
        return_type: string
            one of "matrix" and "quaternion", representation of returned value

        Returns
        -------
        interpolation: array-like of shape [num_samples, ...]
            Geodesic interpolation between x and y.
        """
        r = self.to_rotation(x)
        q = self.to_rotation(y)
        w = interweave(r, q)
        # rotations have timestamps (0, 1, 2, 3 ...), even ones from r, odd from q
        s = Slerp(np.arange(len(w)), w)
        # define timestamps to evaluate at: 0+alpha1, 2+alpha2, ...
        if isinstance(alphas, torch.Tensor):
            alphas = alphas.cpu().numpy()
        coefficients = np.arange(0, len(w), 2) + alphas
        if return_type == "matrix":
            interp = s(coefficients).as_matrix()
        else:
            interp = s(coefficients).as_quat()
        return interp

    def log(self, point, base_point=None, **kwargs):
        """Compute log map between point and base point with result in TS^3.

        Parameters
        ----------
        point: array-like of shape [{num_samples, 1}, {4, [3,3]}]
            target point
        base_point: array-like of shape [{num_samples, 1}, {4, [3,3]}]
            base point

        Returns
        -------
        log: array-like of shape [num_samples, 4]
            Riemannian logarithm in TS^3
        """
        if base_point is None:
            p = self.id(quat=True)
        else:
            p = SO3.to_quaternion(base_point)
        q = SO3.to_quaternion(point)
        dist_pos = self.s3.metric.dist(p, q)[:, None]
        dist_neg = self.s3.metric.dist(p, -q)[:, None]
        q = np.where(dist_neg < dist_pos, -q, q)
        return self.s3.metric.log(q, p)

    def midpoint(self, x, y, return_type="matrix", **kwargs):
        return self.geodesic_interpolation(x, y, 0.5, return_type=return_type)

    def inner_product(self, v, w, keepdims=False):
        """Compute the inner_product between two tangent vectors.

        Parameters
        ----------
        v: np.array of shape [num_samples, {4, [3,3]}]
            tangent vector either in TS^3 or TSO(3)
        w: np.array of shape [num_samples, {4, [3,3]}]
            tangent vector either in TS^3 or TSO(3)

        Returns
        -------
        product: inner product
        """
        if v.shape[1] == 4:
            return np.sum(v * w, axis=1, keepdims=keepdims) * 8 * self.metric_scale
        else:
            return np.sum(v * w, axis=[1, 2], keepdims=keepdims) * self.metric_scale

    def norm(self, v, keepdims=False):
        """Compute the inner_product between two tangent vectors.

        Parameters
        ----------
        v: np.ndarray of shape [num_samples, {4, [3,3]}]
            tangent vector either in TS^3 or TSO(3)

        Returns
        -------
        norm: norm of the vectors
        """
        return self.inner_product(v, v, keepdims=keepdims) ** 0.5

    def random_point(self, num_samples: int, device="cpu", return_type="matrix"):
        """Generate random points on the manifold.

        Parameters
        ----------
        num_samples: int
            Number of samples to generate.
        device: string, default:cpu
            device on which the returned tensor should be located
        return_type: string
            one of "matrix" and "quaternion", representation of returned value

        Returns
        -------
        x: torch.tensor of shape [num_samples, 3, 3]
            Generated random points on manifold.
        """
        samples = R.random(num_samples)
        if return_type == "matrix":
            samples = samples.as_matrix()
        else:
            samples = samples.as_quat()
        return to_torch_and_device(samples, device=device)

    def squared_dist(self, x, y, **kwargs):
        q1 = SO3.to_quaternion(x)
        q2 = SO3.to_quaternion(y)
        return 8 * self.metric_scale * np.arccos(np.abs(np.sum(q1 * q2, axis=1))) ** 2

    def tangent_isometry(
        self, euclidean_vec: torch.Tensor, base_point: torch.Tensor, **kwargs
    ):
        """Isometric map between euclidean space and tangent space of S^3 (not SO(3))

        Computed by computing a basis of tangent space.

        Parameters
        ----------
        euclidean_vec: array-like of shape {[num_samples, 3], [1, 3]}
            vector in euclidean space, either one for each base point or broadcast
        base_point: array-like of shape [{num_samples, 1}, {4,[3,3]}]
            base point, either one for each vector or broadcast

        Returns
        -------
        tangent_vec: torch.tensor of shape [num_samples, 3]
            tangent vector at base point
        """
        if isinstance(euclidean_vec, torch.Tensor):
            euclidean_vec = euclidean_vec.cpu()
        basis = self.tangent_space(base_point)
        tangent_vec = torch.einsum("bi, bik -> bk", euclidean_vec, basis)
        return tangent_vec

    def tangent_space(self, base_point: torch.Tensor) -> torch.Tensor:
        """Compute ONB of tangent space of S^3 (not SO(3)) at base_point.

        Parameters
        ----------
        base_point: tensor of shape [num_samples, 3,3] or [num_samples, 4]
            points on manifold

        Returns
        -------
        basis: tensor of shape [num_samples, 3, 4]
            basis of tangent space at quaternion corresponding to base_point
        """
        q = SO3.to_quaternion(base_point)
        b1 = np.stack([-q[:, 1], q[:, 0], q[:, 3], -q[:, 2]], axis=1)
        b2 = np.stack([-q[:, 2], -q[:, 3], q[:, 0], q[:, 1]], axis=1)
        b3 = np.stack([-q[:, 3], q[:, 2], -q[:, 1], q[:, 0]], axis=1)
        b = np.stack([b1, b2, b3], axis=1) * 1 / (8 * self.metric_scale) ** 0.5
        return torch.tensor(b, dtype=torch.float32)

    @staticmethod
    def to_rotation(x: Union[torch.Tensor, np.ndarray]):
        """Convert tensor to Rotation.

        Parameters
        ----------
        x: array-like of shape [num_samples, 4] or [num_samples, 3, 3]

        Returns
        -------
        x: scipy Rotation
        """
        if isinstance(x, torch.Tensor):
            x = x.cpu().numpy()
        if x.shape[1] == 4:
            return R.from_quat(x)
        else:
            return R.from_matrix(x)

    @staticmethod
    def to_quaternion(x: Union[torch.Tensor, np.ndarray]) -> np.ndarray:
        if isinstance(x, torch.Tensor):
            x = x.cpu().numpy()
        if x.shape[1] != 4:
            return R.from_matrix(x).as_quat()
        else:
            return x

    def uniform_cone(self, num_samples: int, radius=None, radius_min=0.0, device="cpu"):
        return uniform_ball(
            num_samples,
            dim=self.dim,
            radius=radius,
            device=device,
            radius_min=radius_min,
        )


def interweave(r, q) -> R:
    """Interweave two lists of rotations of same length.
    Can either be Rotation object or list of rotation objects.

    Parameters
    ----------
    r: list of rotations or rotation object
    q: list of rotations or rotation object

    Returns
    -------
    w: single rotation object
        interwoven rotations r1, q1, r2, q2, ...
    """
    w = [val for pair in zip(r, q) for val in pair]
    return R.concatenate(w)


def regular_special_orthogonal(
    num_samples_elev=9,
    num_samples_azim=9,
    num_samples_angles=64,
    flatten=True,
    elev_range=None,
    azim_range=(0, 2 * np.pi),
    angles_range=(0, 2 * np.pi),
    end_points=False,
):
    """Sample space of rotations in a regular fashion by sampling upper hemisphere and
    rotation angles.

    Parameters
    ----------
    num_samples_elev: int
        number of samples for elevation angles
    num_samples_azim: int
        number of samples for azimuth angles
    num_samples_angles: int
        number of samples for rotation angles
    flatten: bool
        whether to flatten rotations tensor
    elev_range: tuple of floats
        range of elevation angles
    azim_range: tuple of floats
        range of azimuth angles
    angles_range: tuple of floats
        range of rotation angles
    end_points: bool
        whether to include ends of ranges

    Returns
    -------
    rotations: torch.Tensor of shape
        (n, 3, 3) or
        (num_samples_elev*num_samples_azim, num_samples_angles, 3, 3) if flatten=False
        rotations
    angles: numpy array of shape (n,)
        angles[i] corresponding to rotations[i]
    vectors_index: numpy array of shape (n,)
        vectors[vectors_index[i]] corresponds to rotation axis of rotations[i]
    vectors_xyz: numpy array of shape (num_samples_elev*num_samples_azim, 3)
        all used rotation axes in xyz coordinates
    vectors_polar: numpy array of shape (num_samples_elev*num_samples_azim, 2)
        all used rotation axes in polar coordinates

    where n=num_samples_elev*num_samples_azim*num_samples_angles
    """
    vectors_xyz, vectors_polar = regular_sphere(
        num_samples_elev,
        num_samples_azim,
        upper_hemisphere=False,
        azim_range=azim_range,
        elev_range=elev_range,
        end_points=end_points,
    )
    rot_angles = np.linspace(
        angles_range[0], angles_range[1], num_samples_angles, endpoint=end_points
    )
    vectors_index = []
    angles = []
    rotations = []
    for i in range(len(vectors_xyz)):
        rots = []
        for j in range(len(rot_angles)):
            rot = R.from_rotvec(vectors_xyz[i] * rot_angles[j])
            rots.append(rot)
            vectors_index.append(i)
            angles.append(rot_angles[j])
        rots = R.concatenate(rots)
        rotations.append(rots.as_matrix())
    rotations_tensor = to_torch_and_device(np.stack(rotations, axis=0))
    vectors_index = np.asarray(vectors_index)
    angles = np.asarray(angles)
    if flatten:
        rotations_tensor = rotations_tensor.reshape(-1, 3, 3)
    else:
        rotations_tensor = rotations_tensor.reshape(
            (num_samples_elev, num_samples_azim, num_samples_angles, 3, 3)
        )
    return rotations_tensor, angles, vectors_index, vectors_xyz, vectors_polar


class CowRenderer(Renderer3d):
    def __init__(
        self,
        im_width,
        device,
        dist=2.7,
        elev=90,
        azim=0,
    ):
        file_dir = Path(__file__).parents[3]
        super().__init__(
            im_width,
            device=device,
            dist=dist,
            elev=elev,
            azim=azim,
            obj_filepath=file_dir / "scripts/rotations/cow_mesh/cow.obj",
        )
