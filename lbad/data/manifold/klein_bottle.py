from pathlib import Path

from pytorch3d.renderer import TexturesVertex

from lbad.data.manifold import (
    GeomstatsMidpointManifold,
    MidpointManifoldWithNormalCoordinates,
)
from lbad.data.manifold.klein_bottle_geomstats import KleinBottleGeomstats
from lbad.data.manifold.rendering_utils_3d import Renderer3d
import torch
from matplotlib import cm
import math

from lbad.utils import to_torch_and_device
from lbad.utils.sampling import uniform_ball


class KleinBottle(GeomstatsMidpointManifold, MidpointManifoldWithNormalCoordinates):
    def __init__(self):
        super().__init__(KleinBottleGeomstats(), 2**0.5 / 2)

    def tangent_isometry(self, euclidean_vec, base_point, **kwargs):
        return euclidean_vec

    def uniform_cone(self, num_samples: int, radius=None, radius_min=0.0, device="cpu"):
        return uniform_ball(
            num_samples,
            dim=self.dim,
            radius=radius,
            device=device,
            radius_min=radius_min,
        )


class KleinBottleRenderer:
    def __init__(
        self,
        im_width,
        device,
        obj_filepath=None,
        cmap="spring",
        dist=2.7,
        a=1.0,
        b=1.0,
        elev=90,
        azim=0,
    ):
        self._device = device
        self.a = a
        self.b = b
        if obj_filepath is None:
            file_dir = Path(__file__).parents[3]
            obj_filepath = file_dir / "scripts/klein_bottle/arc_mesh/arc.obj"
        self.renderer = Renderer3d(
            im_width, device, obj_filepath, dist=dist, elev=elev, azim=azim
        )
        self.renderer.textures = self.calculate_textures(cmap)

    def calculate_textures(self, cmap="cool"):
        if isinstance(cmap, str):
            cmap = cm.get_cmap(cmap)
        verts = self.renderer.vertices
        verts_radii = torch.sum(verts**2, dim=1) ** 0.5
        min_radius = min(verts_radii)
        max_radius = max(verts_radii)
        verts_radii_normalized = (verts_radii - min_radius) / (max_radius - min_radius)
        # verts_radii_normalized = verts_radii_normalized.unsqueeze(1)
        verts_rgb = to_torch_and_device(
            cmap(verts_radii_normalized.cpu().numpy()), device=self.device
        )[:, :3]
        verts_rgb = verts_rgb.unsqueeze(0)
        textures = TexturesVertex(verts_features=verts_rgb)
        return textures

    def klein_bottle_to_angles(self, x):
        y = to_torch_and_device(x, device=self.device)
        x = torch.empty_like(y)
        x[:, 0] = y[:, 0] / self.a
        x[:, 1] = y[:, 1] / self.b
        alpha = math.pi * x[:, 0]
        beta = math.pi * (2 * x[:, 1] + 0.5)
        return alpha, beta

    def angles_to_klein_bottle(self, alpha, beta):
        x = alpha / math.pi * self.a
        y = (beta / math.pi - 0.5) / 2 * self.b
        return torch.stack([x, y], dim=1)

    def klein_bottle_to_rotation(self, x):
        alpha, beta = self.klein_bottle_to_angles(x)
        matrices = torch.zeros((len(x), 3, 3)).to(self.device)
        cos_alpha = torch.cos(alpha)
        sin_alpha = torch.sin(alpha)
        cos_beta = torch.cos(beta)
        sin_beta = torch.sin(beta)
        matrices[:, 0, 0] = cos_alpha
        matrices[:, 0, 1] = -sin_alpha * cos_beta
        matrices[:, 0, 2] = sin_beta * sin_alpha
        matrices[:, 1, 0] = sin_alpha
        matrices[:, 1, 1] = cos_alpha * cos_beta
        matrices[:, 1, 2] = -cos_alpha * sin_beta
        matrices[:, 2, 1] = sin_beta
        matrices[:, 2, 2] = cos_beta
        return matrices

    def calculate_mesh(self, x):
        rotations = self.klein_bottle_to_rotation(x)
        return self.renderer.calculate_mesh(rotations)

    def render(self, x, plotting_format=False):
        rotations = self.klein_bottle_to_rotation(x)
        return self.renderer.render(rotations, plotting_format)

    @property
    def device(self):
        return self._device

    @device.setter
    def device(self, value):
        self._device = value
        self.renderer.device = value
