import pickle
from abc import ABC, abstractmethod

import numpy as np
import torch
from torch.utils.data import (
    Dataset,
    DataLoader,
    BatchSampler,
    RandomSampler,
    SequentialSampler,
)

from lbad.utils import to_torch_and_device


class BallSamplerMixin(ABC):
    """
    Mixin class for to supply sampling functionality.
    """

    def __init__(self, sample_tuples, device):
        """Initialize.

        Parameters
        ----------
        sample_tuples: bool
            whether to sample tuples or just points
        device: string
            device on which the returned tensor should be located
        """
        self.sample_tuples = sample_tuples
        self.device = device

    @staticmethod
    def check_eps(eps: float, eps_rel: float, eps_min: float, eps_min_rel: float):
        """Check that eps and eps_rel are valid parameters."""
        if eps is None and eps_rel is None:
            raise ValueError("One of eps and eps_rel should be specified.")
        if eps is not None and eps_rel is not None:
            raise ValueError("Only one of eps and eps_rel should be specified.")
        if eps_min is not None and eps_min_rel is not None:
            raise ValueError("Only one of eps_min and eps_min_rel should be specified.")
        if eps_rel is not None:
            if eps_rel > 1:
                raise ValueError(
                    "eps_rel should be smaller than one, did you mean eps?"
                )
            if eps_rel < 0:
                raise ValueError("maximal distance should be positive.")
        if eps_min_rel is not None:
            if eps_min_rel > 1:
                raise ValueError(
                    "eps_min_rel should be smaller than one, did you mean eps_min?"
                )
            if eps_min_rel < 0:
                raise ValueError("minimal distance should be non-negative.")
        if eps is not None and eps < 0:
            raise ValueError("maximal distance should be positive.")
        if eps_min is not None and eps_min < 0:
            raise ValueError("minimal distance should be non-negative.")
        if (
            eps_rel is not None and eps_min_rel is not None and eps_rel < eps_min_rel
        ) or (eps is not None and eps_min is not None and eps < eps_min):
            raise ValueError("Minimum distance should be smaller than maximum distance")

    def sample(
        self, num_samples, eps=None, eps_rel=None, eps_min=None, eps_min_rel=None
    ):
        """Sample either a tuple of (x, y, distances, interpolations) or just a point x.

        Parameters
        ----------
        num_samples: int
            number of samples to generate
        eps: float
            maximal distance of partner (absolute)
        eps_rel: float between 0 and 1
            maximal distance of partner relative to diameter
        eps_min: float
            minimal distance of partner (absolute)
        eps_min_rel: float between 0 and 1
            minimal distance of partner relative to diameter

        Returns
        -------
        If self.sample_tuples = True:
        samples: dictionary of tuples consisting of
            x: array of shape [num_samples, ...], points on manifold
            y: array of shape [num_samples, ...], points on manifold
            distances: array of shape [num_samples, ], squared distances
            interpolations: array of shape [num_samples, ...], geodesic midpoints
            between x and y
        If self.sample_tuples = False:
        x: array of shape [num_samples, ...]
            samples of points
        """
        if self.sample_tuples:
            x = self.sample_point(num_samples)
            y, distances, interpolations = self.sample_partner(
                x, eps, eps_rel, eps_min, eps_min_rel
            )
            samples = {
                "x": x,
                "y": y,
                "interpolations": interpolations,
                "distances": distances,
            }
            return samples
        else:
            return self.sample_point(num_samples)

    def ball(
        self,
        x: torch.Tensor,
        num,
        eps=None,
        eps_rel=None,
        eps_min=None,
        eps_min_rel=None,
    ):
        """Sample points in a ball around x.

        Parameters
        ----------
        x: tensor of shape [...], no batch dimension
            one point on the manifold
        num: int
            number of samples to generate in ball
        eps: float
            maximal distance of partner (absolute)
        eps_rel: float between 0 and 1
            maximal distance of partner relative to diameter
        eps_min: float
            minimal distance of partner (absolute)
        eps_min_rel: float between 0 and 1
            minimal distance of partner relative to diameter

        Returns
        -------
        y: array of shape [num, ...]
            points in ball around x with radius eps / eps_rel
        """
        x = x.unsqueeze(0).expand(num, *[-1] * x.ndim)
        y, _, _ = self.sample_partner(x, eps, eps_rel, eps_min, eps_min_rel)
        return y

    @abstractmethod
    def sample_partner(
        self,
        x: torch.Tensor,
        eps: float = None,
        eps_rel: float = None,
        eps_min: float = None,
        eps_min_rel: float = None,
    ):
        pass

    @abstractmethod
    def sample_point(self, num_samples: int):
        pass


class MidpointStructure(ABC):
    """
    Abstract base class with bare minimum of functions a manifold should support.
    """

    def __init__(self):
        self.diameter = None
        self.dim = 0
        self.embed_dim = 0

    @abstractmethod
    def random_point(self, num_samples: int, device="cpu"):
        """Generate random points on the manifold.

        Parameters
        ----------
        num_samples: int
            Number of samples to generate.
        device: string, default:cpu
            device on which the returned tensor should be located

        Returns
        -------
        x: np.ndarray of shape [num_samples, ...]
            Generated random points on manifold.
        """
        pass

    @abstractmethod
    def midpoint(self, x, y, **kwargs):
        """Calculate geodesic midpoint between x and y.

        Parameters
        ----------
        x: array-like of shape [num_samples, ...]
            Points on manifold.
        y: array-like of shape [num_samples, ...]
            Points on manifold.

        Returns
        -------
        midpoints: array-like of shape [num_samples, ...]
            Geodesic midpoints between x and y.
        """
        pass

    def dist(self, x, y, **kwargs):
        """Calculate distance between x and y.

        Parameters
        ----------
        x: array-like of shape [num_samples, ...]
            Points on manifold.
        y: array-like of shape [num_samples, ...]
            Points on manifold.

        Returns
        -------
        dist: array-like of shape [num_samples,]
            Distances between points.
        """
        return self.squared_dist(x, y, **kwargs) ** 0.5

    @abstractmethod
    def squared_dist(self, x, y, **kwargs):
        """Calculate squared distance between x and y.

        Parameters
        ----------
        x: array-like of shape [num_samples, ...]
            Points on manifold.
        y: array-like of shape [num_samples, ...]
            Points on manifold.
        Returns
        -------
        squared_dist: array-like of shape [num_samples,]
            Squared distances between points.
        """
        pass


class PrecomputedDatasetWithTransform(Dataset):
    """Dataset based on a dictionary of parameters

    The dictionary should have keys ['x', 'y', 'interpolations', 'distances'], where
    distances are not squared.
    Dictionary can either be passed directly or loaded from a file.

    """

    def __init__(
        self,
        load_path: str = None,
        parameters=None,
        transform=None,
        eps=None,
        sample_tuples=True,
        num_samples=None,
        return_parameters=False,
        scaler=None,
    ):
        """Initialize.

        Parameters
        ----------
        load_path: str, default: None
            path to file where parameter dictionary is stored, loadable by pickle
        parameters: dict, default: None
            dictionary of parameters
        transform: function, default: identity function
            function applied to parameters ['x', 'y', 'interpolations'], e.g. rendering
        eps: float
            maximum distance between pairs
        num_samples: int
            number of samples
        return_parameters: bool
            if true, return both generated images and parameters
        scaler: OneToOneFeatureMixin
            scales parameters before returning them, only relevant if return_parameters
            is True
        """
        self.return_parameters = return_parameters
        self.scaler = scaler
        self.parameters = parameters
        if load_path is None and parameters is None:
            raise ValueError("One of load_path and params should be set.")
        if load_path is not None and self.parameters is not None:
            raise ValueError("Only one of load_path and tensor should be set.")
        if load_path is not None:
            with open(load_path, "rb") as f:
                self.parameters = dict(pickle.load(f))
        self.transform = transform if transform else lambda x: x
        self.eps = eps
        if self.eps is not None:
            self.parameters = self.extract_subset_eps(eps)
        self.sample_tuples = sample_tuples
        self.num_samples = num_samples
        if self.num_samples is not None:
            self.parameters = self.extract_subset(np.arange(num_samples))

    def extract_subset(self, selection):
        """Extract subset of parameter dictionary using only specified indices for each
        array.

        Parameters
        ----------
        selection: [np.ndarray, list]
            array of indices or boolean array

        Returns
        -------
        param_subset: dict
            parameter dictionary of sample pairs

        """
        param_subset = {
            key: np.asarray(value)[selection] for key, value in self.parameters.items()
        }
        return param_subset

    def extract_subset_eps(self, eps):
        """Extract subset of parameter dictionary of all sample pairs with distance
        smaller or equal to eps.

        Parameters
        ----------
        eps: float
            bound on distance

        Returns
        -------
        param_subset: dict
            parameter dictionary of sample pairs

        """
        distances = np.asarray(self.parameters["distances"])
        valid = distances < eps
        return self.extract_subset(valid)

    def __getitem__(self, idx):
        """Return sample dictionary of samples corresponding to idx, containing squared
        distances.

        Parameters
        ----------
        idx: [int, list, np.ndarray]
            indices to return

        Returns
        -------
        if self.sample_tuples and self.return_parameters:
            samples: dict with keys ['x', 'y', 'x_p', 'y_p',
            'interpolations', 'distances']
        if self.sample_tuples and not self.return_parameters:
            samples: dict with keys ['x', 'y', 'interpolations', 'distances']
        if not self.samples_tuples and self.return_parameters:
            samples: dict with keys ['x', 'x_p']
        if not self.sample_tuples and not self.return_parameters:
            samples: tensor containing generated 'x' images
        """
        if self.sample_tuples:
            image_keys = [key for key in self.parameters.keys() if key != "distances"]
        else:
            image_keys = ["x"]
        if np.isscalar(idx):
            idx = [idx]
        param_subset = self.extract_subset(idx)
        samples = {key: self.transform(param_subset[key]) for key in image_keys}
        if self.sample_tuples:
            samples["distances"] = to_torch_and_device(
                param_subset["distances"] ** 2, device=samples["x"].device
            )
        if self.return_parameters:
            for key in image_keys:
                if key in self.parameters.keys():
                    if self.scaler is not None:
                        samples[key + "_p"] = self.scaler.transform(param_subset[key])
                    else:
                        samples[key + "_p"] = param_subset[key]
                    samples[key + "_p"] = to_torch_and_device(
                        samples[key + "_p"], samples[key].device
                    )
        if self.sample_tuples:
            return samples
        else:
            if self.return_parameters:
                return {"x": samples["x"], "x_p": samples["x_p"]}
            else:
                return samples["x"]

    def __len__(self):
        return len(self.parameters["x"])

    def example_batch(self):
        """An example batch used for e.g. visualization.

        Samples 0-5 are the first 6 'x' samples. Samples 6-11 are the first 6 'y'
        samples.
        Samples 12-35 are samples 6-29 of 'x'.

        Returns
        -------
        sample_batch: torch.tensor
            batch of 36 samples
        """
        # 36 images, first and second 6 are used for visualizing interpolations
        samples = self[np.arange(36)]
        sample_batch = torch.empty(
            (36,) + samples["x"][0].shape, device=samples["x"].device
        )
        sample_batch[:6] = samples["x"][:6]
        sample_batch[6:12] = samples["y"][:6]
        sample_batch[12:] = samples["x"][6:30]
        return sample_batch


class MultiIndexDataLoader(DataLoader):
    """
    This DataLoader loads by passing multiple indices to __getitem__ function of
    dataset. To be used with finite,
    indexed datasets, for example test data.

    Inspiration:
    https://discuss.pytorch.org/t/how-to-use-batchsampler-with-getitem-dataset/78788/4
    """

    def __init__(
        self,
        dataset,
        batch_size=1,
        num_workers=0,
        pin_memory=False,
        drop_last=False,
        timeout=0,
        worker_init_fn=None,
        shuffle=True,
    ):
        if shuffle:
            sampler = BatchSampler(
                RandomSampler(dataset), batch_size=batch_size, drop_last=drop_last
            )
        else:
            sampler = BatchSampler(
                SequentialSampler(dataset), batch_size=batch_size, drop_last=drop_last
            )
        super().__init__(
            dataset=dataset,
            num_workers=num_workers,
            sampler=sampler,
            pin_memory=pin_memory,
            timeout=timeout,
            worker_init_fn=worker_init_fn,
            batch_size=None,
        )
        if not drop_last:
            self.num = len(dataset)
        else:
            self.num = len(self) * batch_size
