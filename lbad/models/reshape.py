from torch import nn as nn


class Reshape(nn.Module):
    def __init__(self, *args):
        super(Reshape, self).__init__()
        self.shape = args

    def forward(self, x):
        batch_dim = x.shape[0]
        return x.view((batch_dim,) + self.shape)
