from torch.nn import Sequential, Linear, LeakyReLU

from lbad.models import Reshape
from lbad.utils import initialize


class Regressor(Sequential):
    def __init__(
        self, encoder, layer_dims, input_dim, output_dim, activation=LeakyReLU
    ):
        super().__init__()
        regressor = Sequential()
        layer_dims = [
            input_dim,
        ] + layer_dims
        for i in range(0, len(layer_dims) - 1):
            regressor.add_module(
                f"linear_{i}", Linear(layer_dims[i], layer_dims[i + 1])
            )
            regressor.add_module(f"activation_{i}", activation())
        regressor.add_module(f"linear_{i}", Linear(layer_dims[-1], output_dim))
        initialize(regressor[:-1], activation().negative_slope)
        self.add_module("encoder", encoder)
        self.add_module("reshape", Reshape(input_dim))
        self.add_module("regressor", regressor)
