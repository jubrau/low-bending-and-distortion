import torch
from torch import nn as nn
from torch.nn import Sequential, Conv2d, BatchNorm2d, Linear, Flatten

from lbad.models import Reshape
from lbad.utils import interpolate_along_path, initialize
from lbad.utils import add_batch_dim


class Encoder(nn.Sequential):
    def __init__(
        self,
        num_blocks,
        depth,
        latent,
        colors,
        activation=nn.LeakyReLU,
        batch_norm=False,
        reduce=None,
        pool=True,
        pad=1,
    ):
        """Initialize.

        Parameters
        ----------
        num_blocks: int
            number of convolutional blocks
        depth: int
            number of channels after first convolution (adding channels)
        latent: int
            number of channels for latent code
        colors: int
            number of channels of input image
        activation: function, optional, default: nn.LeakyReLU
            activation function
        batch_norm: bool, optional, default: False
            if true, add batch normalization layer before each activation
        reduce: int or None
            if not None, add a linear layer reducing the number of parameters to
            "reduce"
        pool: {bool, int}
            bool or int, if true include pooling after every convolution
            (except the last),
            if false don't include pooling, if pool is an int, pooling is applied after
            the last 'pool' convolutions (not including the last)
        pad: int, optional, default: 1
            amount of padding in the first convolutional layer
        """
        super().__init__()
        self.add_module(module=nn.Conv2d(colors, depth, 1, padding=pad), name="conv_0")
        k_in = depth  # input features of the block
        if isinstance(pool, bool):
            if pool:
                pool = num_blocks
            else:
                pool = 0
        for i in range(num_blocks):
            k_out = depth * 2**i  # output features of the block
            if not batch_norm:
                self.add_module(
                    module=Sequential(Conv2d(k_in, k_out, 3, padding=1), activation()),
                    name=f"conv_{i + 1}_1",
                )
                self.add_module(
                    module=Sequential(Conv2d(k_out, k_out, 3, padding=1), activation()),
                    name=f"conv_{i + 1}_2",
                )
            else:
                self.add_module(
                    module=Sequential(
                        Conv2d(k_in, k_out, 3, padding=1),
                        BatchNorm2d(k_out),
                        activation(),
                    ),
                    name=f"conv_{i + 1}_1",
                )
                self.add_module(
                    module=Sequential(
                        Conv2d(k_out, k_out, 3, padding=1),
                        BatchNorm2d(k_out),
                        activation(),
                    ),
                    name=f"conv_{i + 1}_2",
                )
            if num_blocks - i <= pool:
                self.add_module(module=nn.AvgPool2d(2), name=f"avg_pool_{i + 1}")
            k_in = k_out
        k_out = depth * 2**num_blocks
        if not batch_norm:
            self.add_module(
                module=Sequential(Conv2d(k_in, k_out, 3, padding=1), activation()),
                name=f"conv_{num_blocks + 1}_1",
            )
        else:
            self.add_module(
                module=Sequential(
                    Conv2d(k_in, k_out, 3, padding=1), BatchNorm2d(k_out), activation()
                ),
                name=f"conv_{num_blocks + 1}_1",
            )
        self.add_module(
            module=Conv2d(k_out, latent, 3, padding=1), name=f"conv_{num_blocks + 1}_2"
        )
        if reduce is not None:
            linear_module = Linear(reduce[0], reduce[1])
            self.add_module(
                module=Sequential(Flatten(), linear_module, Reshape(1, 1, -1)),
                name="linear_reduce",
            )
        initialize(self.modules(), activation().negative_slope)
        if reduce is not None:
            linear_module.reset_parameters()

    def interpolate(self, codes1, codes2, alpha=None):
        """Interpolate between two latent codes.

        Parameters
        ----------
        codes1: torch.tensor
            first latent code
        codes2: torch.tensor
            second latent code
        alpha: float or None, optional, default:None
            interpolation coefficient, if None return function taking alpha as input

        Returns
        -------
        If alpha is not None:
        codes: torch.tensor
            interpolated codes
        Else:
        func: function
            interpolation function

        """

        def interpolation_function(t):
            codes_mixed = torch.lerp(codes1, codes2, t)
            return codes_mixed

        if alpha is None:
            return interpolation_function
        return interpolation_function(alpha)

    def interpolate_images(self, im1, im2, alpha=None):
        """Interpolate two (batches of) images by interpolating in latent space.

        Parameters
        ----------
        im1: tensor of shape {(n_samples, C, H ,W), (C, H ,W)}
            first image
        im2: tensor of shape {(n_samples, C, H ,W), (C, H ,W)}
            second image
        alpha: float
            interpolation coefficient

        Returns
        -------
        interpolations: tensor of shape {(n_samples, l), (1, l)}
            interpolated codes

        """
        im1 = add_batch_dim(im1)
        im2 = add_batch_dim(im2)
        return self.interpolate(self.forward(im1), self.forward(im2), alpha)


class Decoder(nn.Sequential):
    def __init__(
        self,
        num_blocks,
        depth,
        latent,
        colors,
        activation=nn.LeakyReLU,
        batch_norm=False,
        final_activation=None,
        reduce=None,
        upsample=True,
    ):
        """Initialize decoder.

        Parameters
        ----------
        num_blocks: int
            number of convolutional blocks
        depth: int
            number of channels after first convolution (adding channels)
        latent: int
            number of channels for latent code
        colors: int
            number of channels of input image
        activation: function, optional, default: nn.LeakyReLU
            activation function
        batch_norm: bool, optional, default: False
             if true, add batch normalization layer before each activation
        final_activation: function, optional, default: None
            activation function on last layer
        reduce: tuple of ints or None
            if not None, add a linear layer of shape reduce[0] -> reduce[1]
        upsample: bool, optional, default: True
            if true, include upsampling
        """
        super().__init__()
        linear_module = None
        if reduce is not None:
            # full_dim has to be reshaped
            channels = latent
            width = int((reduce[-1] / channels) ** 0.5)
            if len(reduce) == 2:
                # reduce gives (reduced_dim, full_dim)
                linear_module = Linear(reduce[0], reduce[1])
                self.add_module(
                    module=Sequential(linear_module, Reshape(channels, width, width)),
                    name="linear_upscale",
                )
            else:
                for i in range(len(reduce) - 2):
                    self.add_module(
                        module=Sequential(
                            Linear(reduce[i], reduce[i + 1]), activation()
                        ),
                        name=f"upscale_{i}",
                    )
                self.add_module(
                    module=Sequential(
                        Linear(reduce[i], reduce[i + 1]),
                        activation(),
                        Reshape(channels, width, width),
                    ),
                    name=f"upscale_{i}",
                )
        k_in = latent
        for i in range(num_blocks - 1, -1, -1):
            k_out = depth * 2**i
            num = num_blocks - i
            if not batch_norm:
                self.add_module(
                    module=Sequential(Conv2d(k_in, k_out, 3, padding=1), activation()),
                    name=f"conv_{num}_1",
                )
                self.add_module(
                    module=Sequential(Conv2d(k_out, k_out, 3, padding=1), activation()),
                    name=f"conv_{num}_2",
                )
            else:
                self.add_module(
                    module=Sequential(
                        Conv2d(k_in, k_out, 3, padding=1),
                        BatchNorm2d(k_out),
                        activation(),
                    ),
                    name=f"conv_{num}_1",
                )
                self.add_module(
                    module=Sequential(
                        Conv2d(k_out, k_out, 3, padding=1),
                        BatchNorm2d(k_out),
                        activation(),
                    ),
                    name=f"conv_{num}_2",
                )
            if upsample:
                self.add_module(
                    module=nn.Upsample(scale_factor=2), name=f"upsample_{num}"
                )
            k_in = k_out
        if not batch_norm:
            self.add_module(
                module=Sequential(Conv2d(k_in, depth, 3, padding=1), activation()),
                name=f"conv_{num_blocks + 1}_1",
            )
            if final_activation is None:
                self.add_module(
                    module=Sequential(Conv2d(depth, colors, 3, padding=1)),
                    name=f"conv_{num_blocks + 1}_2",
                )
            else:
                self.add_module(
                    module=Sequential(
                        Conv2d(depth, colors, 3, padding=1), final_activation()
                    ),
                    name=f"conv_{num_blocks + 1}_2",
                )
        else:
            self.add_module(
                module=Sequential(
                    Conv2d(k_in, depth, 3, padding=1), BatchNorm2d(depth), activation()
                ),
                name=f"conv_{num_blocks + 1}_1",
            )
            if final_activation is None:
                self.add_module(
                    module=Sequential(Conv2d(depth, colors, 3, padding=1)),
                    name=f"conv_{num_blocks + 1}_2",
                )
            else:
                self.add_module(
                    module=Sequential(
                        Conv2d(depth, colors, 3, padding=1),
                        BatchNorm2d(depth),
                        final_activation(),
                    ),
                    name=f"conv_{num_blocks + 1}_2",
                )
        # assumes leaky relu activation
        initialize(self.modules(), activation().negative_slope)
        if linear_module is not None:
            linear_module.reset_parameters()
        # if final activation is tanh, use different init
        if final_activation is not None and final_activation == torch.nn.Tanh:
            torch.nn.init.xavier_normal_(
                list(list(self.children())[-1].children())[0].weight,
                gain=torch.nn.init.calculate_gain("tanh"),
            )


class Autoencoder(nn.Module):
    """Autoencoder consisting of encoder and decoder."""

    def __init__(self, encoder, decoder):
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, image):
        return self.decoder(self.encoder(image))

    def interpolate(self, codes1, codes2, alpha=None):
        """Interpolate between two latent codes and decode.

        Parameters
        ----------
        codes1: torch.tensor compatible with input shape of decoder
            first latent code
        codes2: torch.tensor compatible with input shape of decoder
            second latent code
        alpha: float or None, optional, default:None
            interpolation coefficient, if None return function taking alpha as input

        Returns
        -------
        If alpha is not None:
        images: torch.tensor
            decoded images
        Else:
        func: function
            interpolation function

        """

        def interpolation_function(t):
            codes_mixed = torch.lerp(codes1, codes2, t)
            out = self.decoder(codes_mixed)
            return out

        if alpha is None:
            return interpolation_function
        return interpolation_function(alpha)

    def interpolate_images(self, im1, im2, alpha=None):
        """Interpolate two (batches of) images by interpolating in latent space and
        decoding.

        Parameters
        ----------
        im1: tensor of shape {(n_samples, C, H ,W), (C, H ,W)}
            first image
        im2: tensor of shape {(n_samples, C, H ,W), (C, H ,W)}
            second image
        alpha: float
            interpolation coefficient

        Returns
        -------
        interpolations: tensor of shape {(n_samples, C, H ,W), (1, C, H ,W)}
            interpolated images

        """
        im1 = add_batch_dim(im1)
        im2 = add_batch_dim(im2)
        return self.interpolate(self.encoder(im1), self.encoder(im2), alpha)

    def interpolate_image_path(self, im1, im2, num=10, process_endpoints=True):
        """Make interpolation path between two (batches of) images.

        Interpolate in latent space and decode, using intermediate steps.

        Parameters
        ----------
        im1: tensor of shape {(n_samples, C, H ,W), (C, H ,W)}
            first image
        im2: tensor of shape {(n_samples, C, H ,W), (C, H ,W)}
            second image
        num: int, optional, default: 10
            number of intermediate steps:
        process_endpoints: bool
            whether to return original endpoints or reconstructed ones

        Returns
        -------
        images: tensor of shape {(n_samples, num+2, C, H, W), (num+2, C, H W)}
            path consisting of start point, intermediate points and end point
        """
        im1 = add_batch_dim(im1)
        im2 = add_batch_dim(im2)
        path = interpolate_along_path(self.interpolate_images)(im1, im2, num=num)
        if process_endpoints:
            path[0] = self(im1)
            path[-1] = self(im2)
        images = torch.stack(path, dim=1).squeeze(0)
        return images

    def num_params(self):
        """Calculate number of parameters.

        Returns
        -------
        num: number of parameters

        """
        return sum(p.numel() for p in self.parameters() if p.requires_grad)
