<div align="center">


  <img src="assets/sundial_figure_final.png" alt="logo" width="700" height="auto" />
    <h1>Low Bending And Distortion Embeddings</h1>
    <p>Code base for the paper "Convergent autoencoder approximation of
  low bending and low distortion manifold embeddings"</p>

<!-- Badges -->
<p>
  <a href="https://gitlab.com/jubrau/LBD/-/graphs/public?ref_type=heads">
    <img src="https://img.shields.io/gitlab/contributors/jubrau/LBD?color=blue&style=for-the-badge" alt="contributors" />
  </a>
  <a href="">
    <img src="https://img.shields.io/gitlab/last-commit/jubrau/LBD?color=orange&gitlab_url=https%3A%2F%2Fgitlab.com&style=for-the-badge" alt="last update" />
  </a>
  <a href="https://gitlab.com/jubrau/LBD/-/blob/public/LICENSE">
    <img src="https://img.shields.io/gitlab/license/jubrau/LBD?style=for-the-badge" alt="license" />
  </a>
  <a href="https://arxiv.org/abs/2208.10193v1">
    <img src="https://img.shields.io/badge/arxiv-2208.10193-red?style=for-the-badge">
</p>
</div>

# About
This is the code base for the paper "Convergent autoencoder approximation of
  low bending and low distortion manifold embeddings".
In this paper, we propose a novel regularization for learning the encoder component of an autoencoder: a loss functional that prefers isometric, extrinsically flat embeddings and allows to train the encoder on its own.
To perform the training it is assumed that for pairs of nearby points on the input manifold their local Riemannian distance and their local Riemannian average can be evaluated. The loss functional is computed via Monte Carlo integration with different sampling strategies for pairs of points on the input manifold. 
Numerical tests, using image data that encodes different explicitly given data manifolds, show that smooth manifold embeddings into latent space are obtained. 
These numerical tests can be reproduced using the code in this repository.

# Getting Started

1. Clone the repository
   `git clone https://gitlab.com/jubrau/LBD.git`
2. Install the requirements in a conda environment
   ```bash
   (base) $ conda create -n lbad python=3.9
   (base) $ conda activate lbad
   (lbad) $ conda install pytorch=1.13.0 torchvision pytorch-cuda=11.6 -c pytorch -c nvidia
   (lbad) $ conda install -c fvcore -c iopath -c conda-forge fvcore iopath
   (lbad) $ conda install pytorch3d=0.7.4 -c pytorch3d
   (lbad) $ conda install matplotlib=3.7 tensorboard=2.12 seaborn=0.12.2
   (lbad) $ conda install -c conda-forge geomstats=2.5
   ```
   The first three install prompts take care of installing PyTorch3D and its dependencies (see [here](https://github.com/facebookresearch/pytorch3d/blob/main/INSTALL.md)).
   
   To generate the images for the Klein Bottle, you should additionally run
   ```bash
   (lbad) $ conda install scikit-image
   (lbad) $ pip install opencv-python-headless
    ```
   
   Most of the evaluation and visualization examples are done in jupyter notebooks. Thus, 
   it is also useful to install a jupyter kernel in the environment. 
   Make sure you have jupyter installed in some environment (for example the base environment).
   If not, using conda this is done by
   ```bash
   (lbad) $ conda activate base
   (base) $ conda install jupyter
   (base) $ conda activate lbad
   ```
   Then install a kernel by running
   ```bash
   (lbad) $ conda install ipykernel
   (lbad) $ ipython kernel install --user --name lbad
   ```
   with the `lbad` environment activated. 
   
# Usage

## Quick Start

To test your installation you can run the jupyter notebook 
`pretrained_models/hemisphere/hemisphere_sundial/encoder/e0.125_f0_rejection/evaluation.ipynb`.

To do this, start jupyter notebook by 
```bash
(base) $ jupyter notebook
```
assuming that jupyter is installed in the `base` conda environment. 
Navigate to the notebook in the jupyter interface in the browser and select the `lbad` kernel (see [Getting Started](#getting-started))
by selecting `Kernel->Change Kernel` in the navigation bar.
You can then run the notebook.

## Training

To reproduce the toy examples as in the paper, specify the desired arguments in a JSON file
and call

`python -m scripts.train -f arguments.json -s save_path -e experiment_name -b base_path -d device --basepathload base_path_load`

from the parent directory of the scripts folder. 

Savepath and experiment name are optional. The experiments will be saved in the folder 
base_path/save_path/experiment_name. The default base_path is `runs`. The default 
save_path is constructed from the manifold and renderer. The experiment name is constructed
from the training parameters. The parameter basepathload is used if the arguments json file contains some 
references to directories, for example for pretrained encoders when training the decoder.

Some example argument configurations are available under `scripts/cylinder`, `scripts/hemisphere` 
and `scripts/rotations`. Support for the
Klein Bottle toy datasets (in this format) is coming soon.

## Loading pretrained models

Some pretrained models and the corresponding parameters can be found under
`pretrained_models`. 
The following script can be used to build an autoencoder or encoder and load the model:

```python
from lbad.utils.training import setup_networks
import json
import torch
from pathlib import Path

# fill in
load_path = Path(...)
device = ...
state_dict_name = ...

with open(f'{load_path}/args.json', 'r') as f:
    args = json.load(f)

if args['architecture']['inlcude_decoder']:
    encoder, decoder, autoencoder = setup_networks(args['architecture'], include_decoder=True)
    autoencoder.to(device)
    network = autoencoder
else:
    encoder = setup_networks(args['architecture'], include_decoder=False)
    encoder.to(device)
    network = encoder

state_dict = torch.load(load_path / state_dict_name)
network.load(state_dict)
```


## Visualizing results

The scripts used to visualize the latent manifolds as in the paper can be found as jupyter 
notebooks in the corresponding subfolder where also the pretrained model is located.
For example, the hemisphere as in the title figure can be generated with the notebook
`pretrained_models/hemisphere/hemisphere_sundial/encoder/e0.125_f0_rejection/evaluation.ipynb`.

## About the code structure
The following diagram shows an overview of the general structures for data generation. 
The most important functions of each class are listed with a “+” and attributes with a “-”. 
Inheritance is visualized by an arrow and association by a diamond. Methods of super classes
which are not overwritten are not relisted.

<div align="center">
   <img src="diagram1.png" alt="UML diagram">
</div>

The abstract class `MidpointStructure` implements the bare minimum of methods needed for our algorithm.
Subclasses of this abstract class are
`MidpointManifoldWithNormalCoordinates` (for manifolds with implemented exponential and logarithm maps) and 
`GeomstatsMidpointManifold`, which can be initialized using any manifold which is implemented in the library [geomstats](https://geomstats.github.io/).
The sampling logic is situated in `ToyDataset`. To each `ToyDataset`, a `MidpointStructure` is associated. For any `MidpointStructure`, the sampling strategies (S1) 
(under the name "rejection_conditional") and (S3) (under the name "rejection") from the paper are implemented.
These strategies might however be very slow, increasingly so for small 𝜖. If the `MidpointStructure` is a `MidpointManifoldWithNormalCoordinates`, 
the sampling strategy (S2) (under the name "tangential") is additionally implemented.
For this sampling strategy, samples are first generated in a ball in Euclidean space and then mapped isometrically to tangent space using the method `tangent_isometry`.
The `ToyDataset` is further responsible for transforming the points on the manifold into  pixel images. This is done using a transform function. 
The transform function usually consists of a rendering process and transferring the generated image to the GPU.

The toy datasets from the paper are then implemented as subclasses `KleinBottle`, `Hemisphere`, `SO3` and `Cylinder` of `MidpointManifoldWithNormalCoordinates`.

To feed the data into the algorithm in batches, we implement a dataloader as in PyTorch. 
The purpose of a dataloader is to iterate through all the elements in a dataset in batches. 
One iteration through a dataset is called an epoch. To work with an “infinite” number of samples, we have the class `InfiniteDataloader`. 
However, similarly to standard dataloaders, one iteration through the `InfiniteDataloader` still consists of finitely many elements, so that we still have epochs. 
However, the generated samples are different in each iteration. To work with precomputed data (for example for testing purposes), we have the class `PrecomputedDatasetWithTransform`. 
One may either precompute just the points on the manifold and add the renderer as a transform, or one may precompute the pixel images. 
This class can be iterated over with the `MultiIndexDataloader`, which generates batches of indices and
loads the corresponding data from the `PrecomputedDatasetWithTransform`.

## Implementing your own toy manifold
If you want to implement your own toy manifold, you should first implement your manifold as a subclass either of `MidpointStructure` or `MidpointManifoldWithNormalCoordinates`.
Additionally, you should implement a renderer class with a method `render(x)`, where `x` is a batch of points from your manifold.
To use the training scripts, add your manifold and renderer to the file `scripts/constants.py` under a name of your choice.
You can then configure an `args.json` file with your own data by specifying the "data" field:

```json
{
   "data": {
           "manifold": "your_manifold_name",
           "manifold_args": {
               "arg1": val1,
               ...    
           }         
           "renderer": "your_renderer_name",
           "renderer_args": {
               "arg1": val1,
               ...
           }
  }
}
```
Here, "renderer_args" are additional arguments needed to initialize your renderer, and "manifold_args" are additional arguments needed to initialize your manifold, as in the constructors of your classes.
